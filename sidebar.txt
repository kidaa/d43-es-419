

----

~~NOCACHE~~ **Door43**

   * [[:es-419:home|Door43 (es-419)]]
  * [[:en:license|Licencia]]

**Recursos**

  * [[:es-419:obs|Historias Bíblicas Libres (es-419)]]

**Descargas**

Todo el contenido para este espacio esta disponible en [[https://github.com/Door43/d43-es-419|d43-es-419 repository]]. Para descargar en archivo zip, pulsar [[https://github.com/Door43/d43-es-419/archive/master.zip|d43-es-419.zip]]

**Cambios Recientes**

{{changes>ns = es-419}}

**Último estado**

{{page>en:uwadmin:es-419:obs:status}}

**Información del Lenguaje**

[[http://www.ethnologue.com/language/es-419|Ethnologue data]]