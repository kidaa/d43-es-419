====== 12. The Exodus ======

====== 12. El Éxodo ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-01.jpg?direct&}}

The Israelites were very happy to leave Egypt. They were no longer slaves, and they were going to the Promised Land! The Egyptians gave the Israelites whatever they asked for, even gold and silver and other valuable things. Some people from other nations believed in God and went along with the Israelites as they left Egypt.

Los israelitas estaban muy contentos por dejar Egipto. ¡Ya no eran esclavos, e iban hacia la tierra prometida! Los egipcios les dieron a los israelitas todo lo que ellos pedían, aún oro,  plata y otras cosas de valor. Algunas personas de otros paises creyeron en Dios y fueron con los israelitas mientras dejaron a Egipto.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-02.jpg?direct&}}

God led them with a tall pillar of cloud that went ahead of them during the day, and which became a tall pillar of fire at night. God was always with them and guided them as they traveled. All they had to do was follow him.

Dios les guió con una columna de nube que iba delante de ellos durante el día, y que llegó se convertía en una columna de fuego de noche. Dios siempre estaba con ellos y les guiaba mientras ellos viajaban. Ellos sólo tenían que seguirle.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-03.jpg?direct&}}

After a short time, Pharaoh and his people changed their minds and wanted the Israelites to be their slaves again. God caused Pharaoh to be stubborn so that people would see he is the One True God, and understand that he, Yahweh, is more powerful than Pharaoh and his gods.

Después de un tiempo corto, el faraón y su gente cambiaron de mente y quisieron que los Israelitas fueran sus esclavos otra vez. Dios endureció al faraón para que la gente viera que Él es el Dios único y verdadero, y que entiendera que Él, Jehováh, es más poderoso que el faraón y sus dioses.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-04.jpg?direct&}}

So Pharaoh and his army chased after the Israelites to make them their slaves again. When the Israelites saw the Egyptian army coming, they realized they were trapped between Pharaoh's army and the Red Sea. They were very afraid and cried out, "Why did we leave Egypt? We are going to die!"

Así que el faraón y su ejercito persiguieron a los israelitas para hacerles sus esclavos otra vez. Cuando los israelitas vieron al ejercito de Egipto acercándose, se dieron cuenta de que estaban atrapados entre el ejército del faraón y el Mar Rojo. Ellos tenían miedo y clamaron, "¿Por qué salimos de Egipto? ¡Vamos a morir!"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-05.jpg?direct&}}

Moses told the Israelites, "Stop being afraid! God will fight for you today and save you." Then God told Moses, "Tell the people to move toward the Red Sea."

Moises dijo a los israelitas, "¡Dejen de tener miedo! Dios peleará para ustedes hoy y les salvará." Entonces, Dios dijo a Moisés, "Dile a la gente que se mueva hacia el Mar Rojo."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-06.jpg?direct&}}

Then God moved the pillar of cloud and placed it between the Israelites and the Egyptians so the Egyptians could not see the Israelites.

Entonces, Dios movió la columna de nube y la colocó entre los israelitas y los egipcios para que los egipcios no pudieran ver a los israelitas.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-07.jpg?direct&}}

God told Moses to raise his hand over the sea and divide the waters. Then God caused wind to push the water in the sea to the left and the right, so that a path formed through the sea.

Dios dijo a Moises que levantara su mano sobre el mar y dividiera las aguas. Entonces, Dios causó que el viento empujara el agua a la izquierda y a la derecha, para que un camino se formara en el mar.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-08.jpg?direct&}}

The Israelites marched through the sea on dry ground with a wall of water on either side of them.

Los israelitas marchaban por el mar en tierra seca con un muro de agua a cada lado de ellos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-09.jpg?direct&}}

Then God moved the cloud up and out of the way so that the Egyptians could see the Israelites escaping. The Egyptians decided to chase after them.

Entonces, Dios mudó la nube arriba y afuera del camino para que los egipcios pudieran ver el escape de los Israelitas. Los egipcios decidieron ir tras de ellos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-10.jpg?direct&}}

So they followed the Israelites onto the path through the sea, but God caused the Egyptians to panic and caused their chariots to get stuck. They shouted, "Run away! God is fighting for the Israelites!"

Así que, ellos siguieron a los israelitas por el camino en el mar, pero Dios causó que los egipcios tuvieran pánico y causó que sus carros se atascaran. Ellos gritaron, "​¡Corran! ¡Dios está peleando por los israelitas!

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-11.jpg?direct&}}

After the Israelites all made it safely to the other side of the sea, God told Moses to stretch out his arm again. When he obeyed, the water fell on the Egyptian army and returned to its normal place. The whole Egyptian army drowned.

Después que llegarán los israelitas seguros a la otra orilla del mar, Dios dijo a Moisés que extendiera su brazo otra vez. Cuando él le obedeció, el agua cayó sobre el ejercito de los egipcios y volvió a su lugar normal. El ejercito entero de Egipto se ahogó.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-12.jpg?direct&}}

When the Israelites saw that the Egyptians were dead, they trusted in God and believed that Moses was a prophet of God.

Cuando los israelitas vieron que los egipcios estaban muertos, ellos confiaron en Dios y creyeron que Moisés era un profeta de Dios.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-13.jpg?direct&}}

The Israelites also rejoiced with much excitement because God had saved them from death and slavery! Now they were free to serve God. The Israelites sang many songs to celebrate their new freedom and to praise God because he saved them from the Egyptian army.

¡Los israelitas se regociaron con mucho ánimo porque Dios les salvó de la muerte y la eclavitud! Ahora estaban libres para servir a Dios. Los Israelitas cantaron muchas canciones para celebrar su libertad nueva y para alabar a Dios porque El les salvó del ejercito Egipcio.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-14.jpg?direct&}}

God commanded the Israelites to celebrate the Passover every year in order to remember how God gave them victory over the Egyptians and rescued them from being slaves. They celebrated by killing a perfect lamb, eating it with unleavened bread.

Dios mandó a los israelitas a que celebraran la Pascua cada año para recordar como Dios les dió la victoria sobre los egipcios y les rescató de ser esclavos. Ellos celebraron al matar un codero perfecto, y comiéndolo con pan sin levadura.

//A Bible story from: Exodus 12:33-15:21//

//Una historia de la Biblia de: Éxodo 12:33-15:21//
