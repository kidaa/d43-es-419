====== 24. John Baptizes Jesus ======

====== 24. Juan Bautiza a Jesús ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-01.jpg?direct&}}

John, the son of Zechariah and Elizabeth, grew up and became a prophet. He lived in the wilderness, ate wild honey and locusts, and wore clothes made from camel hair.

Juan, hijo de Zacarías y Elisabet, creció y se convirtió en un profeta. Vivía en el desierto, comía miel silvestre y langostas, y se vestía con ropa hecha de pelo de camello.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-02.jpg?direct&}}

Many people came out to the wilderness to listen to John. He preached to them, saying, "Repent, for the kingdom of God is near!"

Mucha gente venía al desierto para escuchar a Juan. Él predicó a ellos, diciendo: "¡Arrepiéntanse, porque el reino de Dios está cerca!"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-03.jpg?direct&}}

When people heard John's message, many of them repented from their sins, and John baptized them. Many religious leaders also came to be baptized by John, but they did not repent or confess their sins.

Cuando la gente oía el mensaje de Juan, muchos de ellos se arrepentían de sus pecados y Juan los bautizaba. Muchos líderes religiosos también, llegaron a ser bautizados por Juan, pero no se arrepintieron ni confesaron sus pecados.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-04.jpg?direct&}}

John said to the religious leaders, "You poisonous snakes! Repent and change your behavior. Every tree that does not bear good fruit will be cut down and thrown into the fire." John fulfilled what the prophets said, "See, I send my messenger ahead of you, who will prepare your way."

Juan dijo a los líderes religiosos, "¡Ustedes son serpientes venenosas! Arrepiéntanse y cambien su comportamiento. Todo árbol que no da buen fruto, es cortado y echado en el fuego." Juan cumplió lo que dijeron los profetas: "He aquí, yo envío mi mensajero delante de ti, el cual preparará tu camino."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-05.jpg?direct&}}

Some Jews asked John if he was the Messiah. John replied, "I am not the Messiah, but there is someone coming after me. He is so great that I am not even worthy to untie his sandals."

Algunos judíos preguntaron a Juan si él era el Mesías. Juan respondió: "Yo no soy el Mesías, pero hay alguien que viene después de mí. Él es tan grande que ni siquiera soy digno de desatar sus sandalias."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-06.jpg?direct&}}

The next day, Jesus came to be baptized by John. When John saw him, he said, "Look! There is the Lamb of God who will take away the sin of the world."

Al día siguiente, Jesús vino para ser bautizado por Juan. Cuando Juan lo vio, dijo: "¡Miren! Allí está el Cordero de Dios que quita el pecado del mundo."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-07.jpg?direct&}}

John said to Jesus, "I am not worthy to baptize you. You should baptize me instead." But Jesus said, "You should baptize me, because it is the right thing to do." So John baptized him, even though Jesus had never sinned.

Juan le dijo a Jesús: "Yo no soy digno de bautizarte. En lugar de ello tú deberías bautizarme a mí." Pero Jesús dijo: "Tú debes bautizarme, porque hay que hacer lo que es correcto ." Así que Juan lo bautizó, a pesar de que Jesús nunca había pecado.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-08.jpg?direct&}}

When Jesus came up out of the water after being baptized, the Spirit of God appeared in the form of a dove and came down and rested on him. At the same time, God's voice spoke from heaven, saying, "You are my Son whom I love, and I am very pleased with you."

Cuando Jesús salió del agua después de ser bautizado, el Espíritu de Dios se apareció en la forma de paloma, y ​​vino y se posó sobre él. Al mismo tiempo, la voz de Dios habló desde el cielo, diciendo: "Tú eres mi Hijo, a quien amo, y estoy muy complacido contigo."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-24-09.jpg?direct&}}

God had told John, "The Holy Spirit will come down and rest on someone you baptize. That person is the Son of God." There is only one God. But when John baptized Jesus, he heard God the Father speak, saw God the Son, who is Jesus, and he saw the Holy Spirit.

Dios le dijo a Juan: "El Espíritu Santo vendrá y descansará en alguien que bautizarás. Esa persona es el Hijo de Dios." Sólo hay un Dios. Pero cuando Juan bautizó a Jesús, oyó a Dios el Padre hablando, vio a Dios el Hijo, que es Jesús, y vio al Espíritu Santo.

//A Bible story from: Matthew 3; Mark 1:9-11; Luke 3:1-23//

//Una historia de la Biblia de: Mateo 3; Marcos 1: 9-11; Lucas 3: 1-23//
