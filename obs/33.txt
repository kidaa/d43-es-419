====== 33. The Story of the Farmer ======

====== 33. La Historia del Sembrador ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-01.jpg?direct&}}

One day, Jesus was teaching a very large crowd of people near the shore of the lake. So many people came to hear him that Jesus got into a boat at the edge of the water in order to have enough space to speak to them. He sat in the boat and taught the people.

Un día, Jesús estaba enseñando a una muchedumbre de personas cerca de la orilla del lago. Tanta gente vino para escucharle que Jesús se subió a una barca a la orilla del lago para tener espacio suficiente para hablarles. Él se sentó en la barca y enseñaba a la gente.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-02.jpg?direct&}}

Jesus told this story. "A farmer went out to plant some seed. As he was spreading the seeds by hand, some seeds happened to fall on the path, and the birds came and ate all of those seeds."

Jesús les contó esta historia. "Un agricultor salió a sembrar unas semillas. Mentras estaba esparciendo las semillas a mano, algunas semillas cayeron en el camino, y las aves llegaron y comieron todas aquellas semillas."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-03.jpg?direct&}}

"Other seeds fell on rocky ground, where there was very little soil. The seeds in the rocky ground sprouted quickly, but their roots were not able to go deep into the soil. When the sun came up and it got hot, the plants withered and died."

"Otras semillas cayeron en tierra rocosa, dónde había muy poca tierra. Las semillas en la tierra rocosa brotaron rápidamente, pero sus raíces no pudieron ir profundizar en la tierra. Cuando salió el sol y calentó, las plantas se marchitaron y murieron."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-04.jpg?direct&}}

"Still other seeds fell among thorn bushes. Those seeds began to grow, but the thorns choked them out. So the plants that grew from the seeds in the thorny ground did not produce any grain."

"Otras semillas cayeron entre arbustos espinosos. Aquellas semillas empezaron a crecer, pero las espinas las ahogaron. Así que, las plantas que crecieron de las semillas en la tierra espinosa no produjeron granos."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-05.jpg?direct&}}

"Other seeds fell into good soil. These seeds grew up and produced 30, 60, or even 100 times as much grain as the seed that had been planted. He who has ears, let him hear!"

"Otras semillas cayeron en la buena tierra. Estas semillas crecieron y produjeron 30, 60 y aún 100 veces más granos que la semilla que fue sembrada. ¡El que tiene oídos, oiga!"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-06.jpg?direct&}}

This story confused the disciples. So Jesus explained, "The seed is the word of God. The path is a person who hears God's word, but does not understand it, and the devil takes the word away from him."

Esta historia confundió a los discípulos. Así que, Jesús les explicó. "La semilla es la Palabra de Dios. El camino es la persona que oye la Palabra de Dios, pero no la entiende, y el diablo quita la palabra de él."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-07.jpg?direct&}}

"The rocky ground is a person who hears God's word and accepts it with joy. But when he experiences hardship or persecution, he falls away."

"La tierra rocosa es una persona que escucha la Palabra de Dios y la recibe con gozo. Pero cuando él experimenta las dificultades o las persecuciones, él se vuelve atrás."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-08.jpg?direct&}}

"The thorny ground is a person who hears God's word, but as time passes, the cares, riches, and pleasures of life choke out his love for God. As a result, the teaching he heard does not produce fruit."

"La tierra espinosa es una persona que escucha la Palabra de Dios, pero cuando el tiempo pasa, las preocupaciones, las riquezas, y los placeres de la vida ahogan su amor por Dios. Como resultado, la enseñanza que oyó no produce fruto."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-33-09.jpg?direct&}}

"But the good soil is a person who hears the word of God, believes it, and produces fruit."

"Pero la buena tierra es la persona que escucha la Palabra de Dios, la cree, y produce fruto."

//A Bible story from: Matthew 13:1-8, 18-23; Mark 4:1-8, 13-20; Luke 8:4-15//

//Una historia de la Biblia: Mateo 13:1-8, 18-23; Marcos 4:1-8, 13-20; Lucas 8:4-15//
