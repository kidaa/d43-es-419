====== 20. The Exile and Return ======

====== 20. El Exilio y el Regreso ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-01.jpg?direct&}}

The kingdom of Israel and the kingdom of Judah both sinned against God. They broke the covenant that God had made with them at Sinai. God sent his prophets to warn them to repent and worship him again, but they refused to obey.

Ambos reinos, el reino de Israel y el reino de Judá pecaron contra Dios. Ellos rompieron el pacto que Dios había hecho con ellos en el Sinaí. Dios envió a sus profetas para advertirles que se arrepentieran y le adoraran de nuevo, pero se negaron a obedecer.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-02.jpg?direct&}}

So God punished both kingdoms by allowing their enemies to destroy them. The Assyrian Empire, a powerful, cruel nation, destroyed the kingdom of Israel. The Assyrians killed many people in the kingdom of Israel, took away everything of value, and burned much of the country.

Así que Dios castigó a ambos reinos al permitir que sus enemigos les destruyeran. El Imperio Asirio, una nación cruel y poderosa, destruyó el reino de Israel. Los asirios mataron a muchas personas en el reino de Israel, se llevaron todo lo de valor, y quemaron gran parte del país.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-03.jpg?direct&}}

The Assyrians gathered all the leaders, the rich people, and the people with skills and took them to Assyria. Only the very poor Israelites who had not been killed remained in the kingdom of Israel.

Los asirios se reunieron a todos los dirigentes, la gente rica, y las personas con habilidades y los llevaron a Asíria. Sólo los israelitas muy pobres que no fueron asesinados permanecieron en el reino de Israel.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-04.jpg?direct&}}

Then the Assyrians brought foreigners to live in the land where the kingdom of Israel had been. The foreigners rebuilt the destroyed cities and married the Israelites who were left there. The descendants of the Israelites who married foreigners were called Samaritans.

Entonces, los asirios trajeron extranjeros para vivir en la tierra donde fue el reino de Israel.  Los extranjeros reconstruyeron las ciudades destruidas y se casaron con los israelitas que se quedaron allí. Los descendientes de los israelitas que se casaron con los extranjeros fueron llamados los Samaritanos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-05.jpg?direct&}}

The people in the kingdom of Judah saw how God had punished the people of the kingdom of Israel for not believing and obeying him. But they still worshiped idols, including the gods of the Canaanites. God sent prophets to warn them, but they refused to listen.

La gente en el reino de Judá, vio cómo Dios había castigado a la gente del reino de Israel por no creer y obedecerlo. Pero aún así ellos adoraban ídolos, incluyendo los dioses de los cananeos. Dios envió profetas para advertirles, pero se negaron a escuchar.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-06.jpg?direct&}}

About 100 years after the Assyrians destroyed the kingdom of Israel, God sent Nebuchadnezzar, king of the Babylonians, to attack the kingdom of Judah. Babylon was a powerful empire. The king of Judah agreed to be Nebuchadnezzar's servant and pay him a lot of money every year.

Cerca de 100 años después de que los asirios destruyeron el reino de Israel, Dios envió a Nabucodonosor, el rey de los babilonios, para atacar al reino de Judá.  Babilonia era un imperio poderoso.  El rey de Judá, accedió a ser el siervo de Nabucodonosor y le pagaba una gran cantidad de dinero cada año.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-07.jpg?direct&}}

But after a few years, the king of Judah rebelled against Babylon. So, the Babylonians came back and attacked the kingdom of Judah. They captured the city of Jerusalem, destroyed the Temple, and took away all the treasures of the city and the Temple.

Pero después de algunos años, el rey de Judá se rebeló contra Babilonia. Así que los babilonios volvieron y atacaron el reino de Judá. Ellos capturaron la ciudad de Jerusalén, destruyeron el Templo, y se llevaron todos los tesoros de la ciudad y del Templo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-08.jpg?direct&}}

To punish the king of Judah for rebelling, Nebuchadnezzar's soldiers killed the king's sons in front of him and then made him blind. After that, they took the king away to die in prison in Babylon.

Para castigar al rey de Judá, por rebelarse, los soldados de Nabucodonosor mataron a los hijos del rey frente a él y luego le hizo ciego. Después de eso, se llevaron al rey a morir en una prisión en Babilonia.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-09.jpg?direct&}}

Nebuchanezar and his army took almost all of the people of the kingdom of Judah to Babylon, leaving only the poorest people behind to plant the fields. This period of time when God's people were forced to leave the Promised Land is called the Exile.

Nabucodonosor y su ejército se llevó casi toda la gente del pueblo del reino de Judá a Babilonia, dejando sólo los más pobres para que sembrararán en los campos. Este período de tiempo en que el pueblo de Dios se vio obligado a abandonar la tierra prometida se llama el Exilio.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-10.jpg?direct&}}

Even though God punished his people for their sin by taking them away into exile, he did not forget them or his promises. God continued to watch over his people and speak to them through his prophets. He promised that, after seventy years, they would return to the Promised Land again.

A pesar de que Dios castigó a su pueblo de sus pecados, llevándolos al exilio, no se olvidó de ellos o sus promesas. Dios continuaba vigilando a su pueblo y hablabando con ellos a través de sus profetas. Prometió que después de setenta años ellos regresarían a la Tierra Prometida de nuevo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-11.jpg?direct&}}

About seventy years later, Cyrus, the king of the Persians, defeated Babylon, so the Persian Empire replaced the Babylonian Empire. The Israelites were now called Jews and most of them had lived their whole lives in Babylon. Only a few very old Jews even remembered the land of Judah.

Unos setenta años más tarde, Ciro, el rey de los persas, derrotó Babilonia, por lo que el Imperio Persa reemplazó el Imperio Babilónico. Los israelitas ahora fueron llamados judíos y la mayoría de ellos habían vivido toda su vida en Babilonia. Sólo unos pocos judíos viejos, recordaban la tierra de Judá.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-12.jpg?direct&}}

The Persian Empire was strong but merciful to the people it conquered. Shortly after Cyrus became king of the Persians, he gave an order that any Jew who wanted to return to Judah could leave Persia and go back to Judah. He even gave them money to rebuild the Temple! So, after seventy years in exile, a small group of Jews returned to the city of Jerusalem in Judah.

El Imperio Persa era fuerte, pero misericordioso con las personas que conquistaron. Poco después de que Ciro se convirtió en rey de los persas, dió la orden de que cualquier judío que quisiera regresar a Judá podía dejar Persia y volver a Judá. ¡Incluso les dió dinero para reconstruir el Templo! Así que, después de setenta años de exilio, un pequeño grupo de judíos regresó a la ciudad de Jerusalén en Judá.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-13.jpg?direct&}}

When the people arrived in Jerusalem, they rebuilt the Temple and the wall around the city. Although they were still ruled by other people, once again they lived in the Promised Land and worshiped at the Temple.

Cuando las personas llegaron a Jerusalén, reconstruyeron el templo y el muro alrededor de la ciudad. A pesar de que todavía eran gobernados por otras personas, otra vez vivían en la Tierra Prometida y adoraban en el Templo.

//A Bible story from: 2 Kings 17; 24-25; 2 Chronicles 36; Ezra 1-10; Nehemiah 1-13//

//Una historia de la Biblia de: 2 Reyes 17; 24-25; 2 Crónicas 36; Esdras 1-10; Nehemías 1-13//
