====== 8. Dios Salva a José y a su Familia ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-01.jpg?direct&}}

Many years later, when Jacob was an old man, he sent his favorite son, Joseph, to check on his brothers who were taking care of the herds.

Muchos años después, cuando Jacob era un hombre viejo, él envió a su hijo favorito, José, para ver a sus hermanos quienes estaban cuidando los rebaños.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-02.jpg?direct&}}

Joseph's brothers hated him because their father loved him most and because Joseph had dreamed that he would be their ruler. When Joseph came to his brothers, they kidnapped him and sold him to some slave traders.

Los hermanos de José le odiaban porque su padre le amaba a José más y porque José había soñado que él iba a ser su gobernador. Cuando José llegó a sus hermanos, ellos le secuestraron y le vendieron a algunos traficantes de esclavos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-03.jpg?direct&}}

Before Joseph's brothers returned home, they tore Joseph's robe and dipped it in goat's blood. Then they showed the robe to their father so he would think that a wild animal had killed Joseph. Jacob was very sad.

Antes de que los hermanos de José volvieran a casa, ellos rompieron la túnica de José y la sumergieron en sangre de una cabra. Entonces, le mostraron la túnica a su padre para que pensara que un animal salvaje había matado a José. Jacob estaba muy triste.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-04.jpg?direct&}}

The slave traders took Joseph to Egypt. Egypt was a large, powerful country located along the Nile River. The slave traders sold Joseph as a slave to a wealthy government official. Joseph served his master well, and God blessed Joseph.

Los traficantes de esclavos llevaron a José a Egipto. Egipto era muy grande, un pais poderoso localizado al lado del río Nilo. Los traficantes de esclavos vendieron a José a un oficial rico del gobierno. José servía a su amo bien, y Dios bendijo a José.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-05.jpg?direct&}}

His master's wife tried to sleep with Joseph, but Joseph refused to sin against God in this way. She became angry and falsely accused Joseph so that he was arrested and sent to prison. Even in prison, Joseph remained faithful to God, and God blessed him.

La esposa de su señor intentó dormir con José, pero José rehusó pecar contra Dios de esta manera. Ella se enojó y acusó a José falsamente para que fuera arrestado y enviado a la cárcel. Aún en la cárcel, José se mantenía fiel a Dios y Dios le bendecía.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-06.jpg?direct&}}

After two years, Joseph was still in prison, even though he was innocent. One night, the Pharaoh, which is what the Egyptians called their kings, had two dreams that disturbed him greatly. None of his advisors could tell him the meaning of the dreams.

Después de dos años, José todavía estaba en la carcel, aunque era inocente. Una noche, el Faraón, que es como los Egipcios llamaban a sus reyes, tuvo dos sueños que le pertubaron mucho. Ninguno de sus consejeros podrían decirle el significado de los sueños.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-07.jpg?direct&}}

God had given Joseph the ability to interpret dreams, so Pharaoh had Joseph brought to him from the prison. Joseph interpreted the dreams for him and said, "God is going to send seven years of plentiful harvests, followed by seven years of famine."

Dios había dado a José la habilidad de interpretar los sueños, así, que el Faraón trajo a José de la carcel. José interpretó los sueños para él y le dijo, "Dios va a enviar siete años de cosechas abundantes, seguidas por siete años de hambruna."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-08.jpg?direct&}}

¡El Faraón estaba tan impresionado con José que le nombró como el segundo hombre más poderoso de Egipto!

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-09.jpg?direct&}}

Joseph told the people to store up large amounts of food during the seven years of good harvests. Then Joseph sold the food to the people when the seven years of famine came so they would have enough to eat.

José dijo a la gente que almacenarán grandes cantidades de comida durante los siete años de las cosechas buenas. Entonces, José vendía la comida a la gente cuando llegaron los siete años de hambruna y así tuvieran suficiente para comer.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-10.jpg?direct&}}

The famine was severe not only in Egypt, but also in Canaan where Jacob and his family lived.

El hambruna era muy severa no sólo en Egipto, pero también,en Canaan dónde Jacob y su familia vivían.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-11.jpg?direct&}}

Así, Jacob envió a sus hijos mayores a Egipto para comprar comida. Los hermanos no reconocieron a José cuando ellos estuvieron de pie ante él para comprar comida. Pero José les reconoció.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-12.jpg?direct&}}

After testing his brothers to see if they had changed, Joseph said to them, "I am your brother, Joseph! Do not be afraid. You tried to do evil when you sold me as a slave, but God used the evil for good! Come and live in Egypt so I can provide for you and your families."

Después de probar a sus hermanos para ver si habían cambiado, José les dijo, "¡Yo soy su hermano José! No tengan miedo. Ustedes intentaron hacerme mal cuando me vendieron como esclavo, pero Dios ha usado el mal para el bien! Vengan y vivan en Egipto para que yo pueda proveer para ustedes y sus familias."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-13.jpg?direct&}}

Cuando los hermanos de José volvieron a casa y le dijeron a su padre, Jacob, que José estaba todavía vivo, él estuvo muy contento.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-14.jpg?direct&}}

Aunque Jacob era un hombre viejo, él se mudó a Egipto con toda su familia, y todos vivían allá. Antes de morir Jacob, él bendijo cada uno de sus hijos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-15.jpg?direct&}}

The covenant promises that God gave to Abraham were passed on to Isaac, then to Jacob, and then to Jacob's twelve sons and their families. The descendants of the twelve sons became the twelve tribes of Israel.

Las promesas del pacto que Dios le dió a Abraham fueron dadas a Isaac, luego a Jacob, y luego a los doce hijos y a sus familias. Los descendientes de los doce hermanos llegaron a ser las doce tribus de Israel.

//A Bible story from: Genesis 37-50//

//Una historia de la Biblia de: Génesis 37-50//
