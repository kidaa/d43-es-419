====== 27. The Story of the Good Samaritan ======

====== 27. La Historia del Buen Samaritano ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-01.jpg?direct&}}

One day, an expert in the Jewish law came to Jesus to test him, saying, "Teacher, what must I do to inherit eternal life?" Jesus answered, "What is written in God's law?"

Un día, un experto en la ley judía se acercó a Jesús para ponerlo a prueba, diciendo: "Maestro, ¿qué debo hacer para heredar la vida eterna?" Jesús le respondió: "¿Qué está escrito en la ley de Dios?"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-02.jpg?direct&}}

The law expert replied that God's law says, "Love the Lord your God with all your heart, soul, strength, and mind. And love your neighbor as yourself." Jesus answered, "You are correct! Do this and you will live."

El experto de la ley respondió que la ley de Dios dice: "Amarás al Señor tu Dios con todo tu corazón, tu alma, tu fuerza y ​​tu mente. Y amarás a tu prójimo como a tí mismo." Jesús le respondió: "¡Estás en lo correcto! Haz esto y vivirás."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-03.jpg?direct&}}

But the law expert wanted to prove that he was righteous, so he asked, "Who is my neighbor?"

Pero el experto de la ley quería demostrar que el era justo, así que, le preguntó: "¿Quién es mi prójimo?"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-04.jpg?direct&}}

Jesus answered the law expert by telling a story. "There was a Jewish man who was traveling along the road from Jerusalem to Jericho."

Jesús respondió al experto de la ley contándole una historia. "Había un hombre judío que viajaba por el camino de Jerusalén a Jericó."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-05.jpg?direct&}}

"While the man was traveling, he was attacked by a group of robbers. They took everything he had and beat him until he was almost dead. Then they went away."

"Mientras el hombre estaba de viaje, fue atacado por un grupo de ladrones. Se llevaron todo lo que tenía y lo golpearon hasta dejarlo casi muerto. Luego se fueron."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-06.jpg?direct&}}

"Soon after that, a Jewish priest happened to walk down that same road. When this religious leader saw the man who had been robbed and beaten, he moved to the other side of the road, ignored the man who needed help, and kept on going."

"Poco después de eso, un sacerdote judío pasó caminando por ese mismo camino. Cuando este líder religioso vio al hombre que había sido asaltado y golpeado, se movió al otro lado del camino, ignorando al hombre que necesitaba ayuda, y continuó su camino".

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-07.jpg?direct&}}

"Not long after that, a Levite came down the road. (Levites were a tribe of Jews who helped the priests at the Temple.) The Levite also crossed over to the other side of the road and ignored the man who had been robbed and beaten."

"No mucho tiempo después de eso, un levita llegó por el camino. (Los Levitas eran una tribu de judíos que ayudaban a los sacerdotes en el Templo.) El levita también, cruzó al otro lado del camino e ignoró el hombre que había sido asaltado y golpeado ".

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-08.jpg?direct&}}

"The next person to walk down that road was a Samaritan. (Samaritans were the descendants of Jews who had married people from other nations. Samaritans and Jews hated each other.) But when the Samaritan saw the Jewish man, he felt very strong compassion for him. So he cared for him and bandaged his wounds."

"La próxima persona que bajo por ese camino era un samaritano.  (Los samaritanos eran descendientes de los judíos que se habían casado con personas de otras naciones. Los samaritanos y los judíos se odiaban entre sí.)  Pero cuando el samaritano vio al hombre judío, el sintío mucha compasión por él. Así que, él cuidó de él y vendó sus heridas ".

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-09.jpg?direct&}}

"The Samaritan then lifted the man onto his own donkey and took him to a roadside inn where he took care of him."

"El Samaritano luego subió al hombre a su propio burro y lo llevó a un mesón y donde cuidó de él."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-10.jpg?direct&}}

"The next day, the Samaritan needed to continue his journey. He gave some money to the person in charge of the inn and said, 'Take care of him, and if you spend any more money than this, I will repay those expenses when I return.'"

"Al día siguiente, el samaritano tenía que continuar su viaje. Le dio un poco de dinero a la persona encargada de la posada y le dijo: 'Cuida de él y, si gastas más dinero que esto, yo le pagaré los gastos a mi regreso. '"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-27-11.jpg?direct&}}

Then Jesus asked the law expert, "What do you think? Which one of the three men was a neighbor to the man who was robbed and beaten?" He replied, "The one who was merciful to him." Jesus told him, "You go and do the same."

Entonces, Jesús preguntó al experto de la ley, "¿Qué piensas?" "¿Cuál de los tres hombres fue el prójimo para el hombre que fue asaltado y golpeado?"  El respondió, "El hombre que le mostró misericordia." Jesús de dijo, "Vé y haz lo mismo."

//A Bible story from: Luke 10:25-37//

//Una historia de la Biblia de: Lucas 10:25-37//
