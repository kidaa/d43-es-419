====== 35. The Story of the Compassionate Father ======

====== 35. La Historia del Padre Compasivo ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-01.jpg?direct&}}

One day, Jesus was teaching many tax collectors and other sinners who had gathered to hear him.

Un día, Jesús estaba enseñado a muchos cobradores de impuestos y a otros pecadores que se juntaron para escucharle.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-02.jpg?direct&}}

Some religious leaders who were also there saw Jesus treating these sinners as friends, and they began to criticize him to each other. So Jesus told them this story.

Algunos líderes religiosos que estaban allí también, vieron a Jesús tratando a los pecadores como amigos, y ellos empezaron a criticarle entre ellos. Así que, Jesús les contó esta historia.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-03.jpg?direct&}}

"There was a man who had two sons. The younger son told his father, 'Father, I want my inheritance now!' So the father divided his property between his two sons."

"Había un hombre que tenía dos hijos. El hijo menor dijo a su padre, "¡Padre, quiero mi herencia ya!"  Así que, el Padre dividió su propiedad entre sus dos hijos."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-04.jpg?direct&}}

"Soon the younger son gathered all that he had and went far away and wasted his money in sinful living."

"Pronto el hijo menor tomó todo que tenía y se fue lejos y malgastó su dinero en una vida pecaminosa."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-05.jpg?direct&}}

"After that, a severe famine happened in the land where the younger son was, and he had no money to buy food. So he took the only job he could find, feeding pigs. He was so miserable and hungry that he wanted to eat the pigs' food."

"Después de aquello, ocurrió una hambruna en la tierra donde el hijo menor estaba, y él no tenía dinero para comprar comida. Así que, él tomó el único trabajo que pudo encontrar, dando comida a los cerdos. El era tan miserable y estaba tan hambriento que él quería comerse la comida de los cerdos."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-06.jpg?direct&}}

"Finally, the younger son said to himself, 'What am I doing? All my father's servants have plenty to eat, and yet here I am starving. I will go back to my father and ask to be one of his servants.'"

Finalmente, el hijo menor se dijo a sí mismo, "¿Qué estoy haciendo? Todos los siervos de mi padre tienen bastante para comer, y sin embargo aquí estoy yo muriendo de hambre. Volveré a mi padre y le pediré ser uno de sus siervos."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-07.jpg?direct&}}

"So the younger son started back towards his father's home. When he was still far away, his father saw him and felt compassion for him. He ran to his son and hugged him and kissed him."

"Así que, el hijo menor empezó su camino de regreso al hogar de su padre. Cuando todavía estuvo lejos, su padre lo vió y sintió compasión por él. El corrió a su hijo y le abrazó y le besó."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-08.jpg?direct&}}

"The son said, 'Father, I have sinned against God and against you. I am not worthy to be your son.'"

"El hijo dijo, 'Padre, he pecado contra Dios y contra ti.  No soy digno de ser tu hijo.'"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-09.jpg?direct&}}

"But his father told one of his servants, 'Go quickly and bring the best clothes and put them on my son! Put a ring on his finger and put sandals on his feet. Then kill the best calf so we can have a feast and celebrate, because my son was dead, but now he is alive! He was lost, but now he is found!'"

"Pero su padre le dijo a uno de sus siervos, "¡Vé rápido y trae la mejor ropa y víste a mi hijo con ella! Pon un anillo en su dedo y pon sandalias en sus pies. ¡Entonces, mata el mejor becerro para que podamos hacer fiesta y celebrar, porque mi hijo estaba muerto, pero ahora está vivo! ¡Estaba perdido, pero ahora es hallado!'"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-10.jpg?direct&}}

"So the people began to celebrate. Before long, the older son came home from working in the field. He heard the music and dancing and wondered what was happening."

"Así que, la gente empezó a celebrar. Al poco tiempo, el hijo mayor llegó a la casa de trabajar en el campo. El oyó la música y las danzas y se preguntó acerca de lo que estaba sucediendo."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-11.jpg?direct&}}

"When the older son found out that they were celebrating because his brother had come home, he was very angry and would not go into the house. His father came out and begged him to come and celebrate with them, but he refused."

"Cuando el hijo mayor se dió cuenta que estaban celebrando porque su hermano había llegado a casa, él estaba enojado y no quería entrar en la casa. Su padre salió y le rogó que entrara para celebrar con ellos, pero él se rehusó."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-12.jpg?direct&}}

"The older son said to his father, 'All these years I have worked faithfully for you! I never disobeyed you, and still you did not even give me one small goat so I could celebrate with my friends. But when this son of yours who consumed your money in sinful behavior came home, you killed the best calf for him!'"

"El hijo mayor dijo a su padre, '¡Todos estos años he trabajado fielmente para ti! Nunca te desobedeceria, y aun así no me has dado ni una pequeña cabra para que yo pudiera celebrar con mis amigos. ¡Pero cuando este hijo tuyo que malgastó tu dinero en una vida pecaminosa llegó a casa, mataste el mejor becerro para él!

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-35-13.jpg?direct&}}

"The father answered, 'My son, you are always with me, and everything I have is yours. But it is right for us to celebrate, because your brother was dead, but now is alive. He was lost, but now is found!'"

"El padre le contestó, 'Hijo, siempres estás conmigo, y todo lo que tengo es tuyo.  Pero es justo que celebremos, porque tu hermano estaba muerto, pero ahora está vivo. ¡Estaba perdido, pero ahora es hallado!'"

//A Bible story from: Luke 15:11-32//

//Una historia de: Lucas 15:11-32//
