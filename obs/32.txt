====== 32. Jesus Heals a Demon-Possessed Man & a Sick Woman ======

====== 32. Jesus Sana un hombre Endemoniado y a una Mujer Enferma ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-01.jpg?direct&}}

One day, Jesus and his disciples went in a boat across the lake to the region where the Gerasene people lived.

Un día, Jesús y sus discípulos cruzaron el lago en un bote a la región donde la gente de Gadara vivía.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-02.jpg?direct&}}

When they reached the other side of the lake, a demon-possessed man came running up to Jesus.

Cuando ellos llegaron al otro lado del lago, un hombre endemoniado vino corriendo a Jesús.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-03.jpg?direct&}}

This man was so strong that nobody could control him. People had even fastened his arms and legs with chains, but he kept breaking them.

El hombre estaba tan fuerte que nadie podía controlarle. La gente incluso había sujetado sus brazos y piernas con cadenas, pero él las rompía.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-04.jpg?direct&}}

The man lived among the tombs in the area. The man would scream all day and all night. He did not wear clothes and he cut himself repeatedly with stones.

El hombre vivía en los sepulcros en el área. Este hombre gritaba todo el día y toda la noche. El no usaba ropa y se cortaba con piedras repetidamente.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-05.jpg?direct&}}

When the man came to Jesus, he fell on his knees in front of him. Jesus said to the demon, "Come out of this man!"

Cuando el hombre vino a Jesús, el cayó de rodillas ante Él. Jesús le dijo al demonio, "¡Sal de este hombre!"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-06.jpg?direct&}}

The man with the demon cried out in a loud voice, "What do you want with me, Jesus, Son of the Most High God? Please do not torture me!" Then Jesus asked the demon, "What is your name?" He replied, "My name is Legion, because we are many." (A "legion" was a group of several thousand soldiers in the Roman army.)

El hombre con el demonio gritó en voz alta, "¿Que quieres conimgo , Jesús, Hijo del Dios Altísimo? ¡Por favor, no me tortures!" Entonces, Jesús preguntó al demonio, "¿Cómo te llamas?" El respondió, "Me llamo Legión, porque somos muchos." (Una "legión" era un grupo de varios millares de soldados en el ejército Romano.)

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-07.jpg?direct&}}

The demons begged Jesus, "Please do not send us out of this region!" There was a herd of pigs feeding on a nearby hill. So, the demons begged Jesus, "Please send us into the pigs instead!" Jesus said, "Go!"

Los demonios rogaron a Jesús, "¡Por favor, no nos envíes fuera de esta región!" Había una manada de cerdos comiendo en una ​colina cercana. Así que, los demonios rogaron a Jesús, "Por favor, envíanos a los cerdos!" Jesús les dijo, "¡Vayan!"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-08.jpg?direct&}}

The demons came out of the man and entered the pigs. The pigs ran down a steep bank into the lake and drowned. There were about 2,000 pigs in the herd.

Los demonios salieron del hombre y entraron en los cerdos. Los cerdos corrieron por un despeñadero al lago y se ahogaron. Había como 2,000 cerdos en la manada.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-09.jpg?direct&}}

When the people who took care of the pigs saw what happened, they ran into the town and told everyone that they met what Jesus had done. The people from the town came and saw the man who used to have the demons. He was sitting calmly, wearing clothes, and acting like a normal person.

Cuando la gente que cuidaban los cerdos vieron lo que pasó, ellos corrieron al pueblo y dijeron ​lo que Jesús hizo a toda la gente que encontraron. La gente del pueblo vino y vió al hombre que antes tenía los demonios. Estaba sentado con calma, vestido, y actuando como una persona normal.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-10.jpg?direct&}}

The people were very afraid and asked Jesus to leave. So Jesus got into the boat and prepared to leave. The man who used to have the demons begged to go along with Jesus.

La gente tenía mucho miedo y pidió a Jesús que se fuera. Así que, Jesús subió a la barca y se preparó para irse. El hombre que antes tenía los demonios rogaba a Jesús permiso para ir con él.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-11.jpg?direct&}}

But Jesus said to him, "No, I want you to go home and tell your friends and family about everything that God has done for you and how he has had mercy on you."

Pero Jesús le dijo, "No, quiero que vayas a tu casa y digas a tus amigos y a tu familia acerca de todo lo que Dios ha hecho por ti y cómo Él te ha mostrado misericordia."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-12.jpg?direct&}}

So the man went away and told everyone about what Jesus had done for him. Everyone who heard his story was filled with wonder and amazement.

Así que, el hombre se fue y hablaba a todos acerca de lo que Jesús había hecho por él. Todos los que escucharon su historia se llenaban de admiración y asombro.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-13.jpg?direct&}}

Jesus returned to the other side of the lake. After he arrived there, a large crowd gathered around him and pressed in on him. In the crowd was a woman who had suffered from a bleeding problem for twelve years. She had paid all of her money to doctors so they would heal her, but she only got worse.

Jesus volvió al otro lado del lago. Después de llegar, una muchedumbre grande le seguía y le presionaba. En el grupo había una mujer que había sufrido de un problema de sangrado por doce años. Ella había gastado todo su dinero en médicos para que le sanaran, pero ella solamente empeoraba.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-14.jpg?direct&}}

She had heard that Jesus had healed many sick people and thought, "I'm sure that if I can just touch Jesus' clothes, then I will be healed, too!" So she came up behind Jesus and touched his clothes. As soon as she touched them, the bleeding stopped!

Ella había escuchado que Jesús había sanado a muchos enfermos y pensaba, "Estoy segura que si sólo puedo tocar su ropa, entonces, seré sanada también!" Así que, ella se acercó a Jesús por detrás y tocó su ropa. ¡Tan pronto ella le tocó, el sangrado se detuvó!

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-15.jpg?direct&}}

Immediately, Jesus realized that power had gone out from him. So he turned around and asked, "Who touched me?" The disciples replied, "There are many people crowding around you and bumping into you. Why did you ask, 'Who touched me?'"

Inmediatamente, Jesús se dio cuenta que poder había salido de Él.  Así que, se volteo y preguntó, "¿Quién me tocó?" Los discípulos le respondieron, "Hay mucha gente cerca de tí y presionándote. Por qué preguntas, "¿Quién te tocó?"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-16.jpg?direct&}}

The woman fell on her knees before Jesus, shaking and very afraid. Then she told him what she had done, and that she had been healed. Jesus said to her, "Your faith has healed you. Go in peace."

La mujer se puso de rodillas ante Jesús, temblando y muy temerosa. Entonces, ella le dijo lo que había hecho, y que estaba sanada. Jesús le dijo, "Tu fe te ha sanado. Vé en paz."

//A Bible story from: Matthew 8:28-34; 9:20-22; Mark 5:1-20; 5:24b-34; Luke 8:26-39; 8:42b-48//

//​Una historia de: Mateo 8:28-34; 9:20-22; Marcos 5:1-20; 5:24b-34; Lucas 8:26-39; 8:42b-48//
