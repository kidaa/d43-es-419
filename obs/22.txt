====== 22. The Birth of John ======

====== 22. El Nacimiento de Juan ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-01.jpg?direct&}}

In the past, God had spoken to his people through his angels and prophets. But then 400 years went by when he did not speak to them. Suddenly an angel came with a message from God to an old priest named Zechariah. Zechariah and his wife, Elizabeth, were godly people, but she had not been able to have any children.

En el pasado, Dios había hablado a su pueblo a través de sus ángeles y profetas. Pero 400 años pasaron en los cuales Él no habló con ellos. De repente, un ángel vino con un mensaje de Dios a un viejo sacerdote llamado Zacarías.  Zacarías y su esposa, Elisabet, eran personas piadosas, pero no habían podido tener hijos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-02.jpg?direct&}}

The angel said to Zechariah, "Your wife will have a son. You will name him John. He will be filled with the Holy Spirit, and will prepare the people for the Messiah!" Zechariah responded, "My wife and I are too old to have children! How will I know this will happen?"

El ángel le dijo: Zacarías, "Tu mujer tendrá un hijo. Le pondrás por nombre Juan. ¡Él será lleno del Espíritu Santo, y preparará al pueblo para el Mesías!" Zacarías respondió: "¡Mi esposa y yo somos demasiado viejos para tener hijos! ¿Cómo sabré que esto va a suceder?"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-03.jpg?direct&}}

The angel responded to Zechariah, "I was sent by God to bring you this good news. Because you did not believe me, you will not be able to speak until the child is born." Immediately, Zechariah was unable to speak. Then the angel left Zechariah. After this, Zechariah returned home and his wife became pregnant.

El ángel respondió a Zacarías, "He sido enviado por Dios para darte estas buenas noticias. Porque no me creiste, no podrás hablar hasta que nazca el niño."  Inmediatamente, Zacarías fue incapaz de hablar. Y el ángel dejó a Zacarías. Después de esto, Zacarías regresó a su casa y su esposa quedó embarazada.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-04.jpg?direct&}}

When Elizabeth was six months pregnant, the same angel suddenly appeared to Elizabeth's relative, whose name was Mary. She was a virgin and was engaged to be married to a man named Joseph. The angel said, "You will become pregnant and give birth to a son. You are to name him Jesus. He will be the Son of the Most High God and will rule forever."

Cuando Elisabet estaba embarazada de seis meses, el mismo ángel apareció de repente a una pariente de Isabel, que se llamaba María. Ella era virgen y estaba comprometida para casarse con un hombre llamado José. El ángel le dijo: "Vas a quedar embarazada y darás a luz un hijo. Vas a llamarle Jesús. Él será el Hijo del Dios Altísimo y reinará para siempre."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-05.jpg?direct&}}

Mary replied, "How can this be, since I am a virgin?" The angel explained, "The Holy Spirit will come to you, and the power of God will overshadow you. So the baby will be holy, the Son of God." Mary believed and accepted what the angel said.

María respondió: "¿Cómo será esto, puesto que soy virgen?"  El ángel le explicó: "El Espíritu Santo vendrá a tí, y el poder de Dios se posará sobre tí.   Así que el bebé va a ser santo, el Hijo de Dios." María creyó y aceptó lo que el ángel dijo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-06.jpg?direct&}}

Soon after the angel spoke to Mary, she went and visited Elizabeth. As soon as Elizabeth heard Mary's greeting, Elizabeth's baby jumped inside her. The women rejoiced together about what God had done for them. After Mary visited Elizabeth for three months, Mary returned home.

Poco después de que el ángel habló a María, ella fue y visitó a Elisabet. En cuanto Elisabet oyó el saludo de María, el bebé de Elisabet saltó dentro de ella. Las mujeres se regocijaron juntas acerca de lo que Dios había hecho por ellas. Después de estar María con Elisabet por tres meses, María regresó a su casa.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-07.jpg?direct&}}

After Elizabeth gave birth to her baby boy, Zechariah and Elizabeth named the baby John, as the angel had commanded. Then God allowed Zechariah to speak again. Zechariah said, “Praise God, because he has remembered his people! You, my son, will be called the prophet of the Most High God who will tell the people how they can receive forgiveness for their sins!”

Después de dar a luz Elisabet a su bebé, Zacarías e Elisabet nombraron al bebé Juan, como el ángel le había mandado. Entonces, Dios permitió que Zacarías volviera a hablar. Zacarías dijo: "¡Alabado sea Dios, porque Él se ha acordado de su pueblo! ¡Tú, hijo mío, serás llamado profeta del Dios Altísimo quien dirá a la gente cómo pueden recibir el perdón de sus pecados! "

//A Bible story from: Luke 1//

//Una historia de la Biblia de: Lucas 1//
