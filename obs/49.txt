====== 49. God’s New Covenant ======

====== 49. El Pacto Nuevo de Dios ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-01.jpg}}

An angel told a virgin named Mary that she would give birth to God's Son. So while she was still a virgin, she gave birth to a son and named him Jesus. Therefore, Jesus is both God and human.

Un ángel dijo a una virgen se llamaba María que ella iba a dar a a luz al Hijo de Dios. Así, minetras todavía era una virgen, ella dio a luz a un hijo y le nombró Jesús. Por lo tanto, Jesús es ambos Dios y hombre.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-02.jpg}}

Jesus did many miracles that prove he is God. He walked on water, calmed storms, healed many sick people, drove out demons, raised the dead to life, and turned five loaves of bread and two small fish into enough food for over 5,000 people.

Jesús hizo muchos milagros para probar que es Dios. El caminó encima del agua , calmó las tormentas, sanó a muchas peersonas enfermas, echó fuera demonios, levantó los muertos a la vida, y cambió cinco panes y dos pescados pequeños a suficiente comida para dar a comer a 5,000 personas.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-03.jpg}}

Jesus was also a great teacher, and he spoke with authority because he is the Son of God. He taught that you need to love other people the same way you love yourself.

Jesús era también, un gran maestro, y él habló con autoridad porque era el Hijo de Dios. El enseñó que debemos amar a los demás como a nosotros mismos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-04.jpg}}

He also taught that you need to love God more than you love anything else, including your wealth.

El también, enseñó que tenemos que amar a Dios más que amamos a cualquier otra cosa, incluyendo nuestras riquezas.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-05.jpg}}

Jesus said that the kingdom of God is more valuable than anything else in the world. The most important thing for anyone is to belong to the kingdom of God. To enter into God's kingdom, you must be saved from your sin.

Jesús dijo que el reino de Dios vale más que cualquier otra cosa en el mundo. La cosa más importante para qualquier persona es pertenecer al reino de Dios. Pare entrar en el reino de Dios, Ud. tiene que ser salvado de su pecado.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-06.jpg}}

Jesus taught that some people will receive him and be saved, but others will not. He said that some people are like good soil. They receive the good news of Jesus and are saved. Other people are like the hard soil of a path, where the seed of God's word does not enter and does not produce any harvest. Those people reject the message about Jesus and will not enter into his kingdom.

Jesús enseñó que algunas personas le recibir án y serás salvadas, pero otras no serán salavadas. El dijo que alguna gente es como la tierra buena. Ellos reciben las buenas nuevas de Jesús y son salvados. Otras personas son como la tierra dura de un camino, donde la semilla de la palabra de Dios no penetra y no produce una cosecha. Aquellas personas niegan el mensaje de jesús y no entrarán en su reino.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-07.jpg}}

Jesus taught that God loves sinners very much. He wants to forgive them and to make them his children.

Jesús enseñó que Dios ama a los pecadores mucho. El quiere perdonarles y hacerles sus hijos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-08.jpg}}

Jesus also told us that God hates sin. When Adam and Eve sinned, it affected all of their descendants. As a result, every person in the world sins and is separated from God. Therefore, everyone has become an enemy of God.

Jesús también, nos dijo que Dios odia el pecado. Cuando Adán y Eva pecaron, esto afectó a todos sus decendientes. Como resultado, toda persona en el mundo peca y está separada de Dios. Por lo tanto, toda persona ha llegado a ser un enemigo de Dios.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-09.jpg}}

But God loved everyone in the world so much that he gave his only Son so that whoever believes in Jesus will not be punished for his sins, but will live with God forever.

Pero Dios amaba a todos en el mundo tanto que El dio a su único Hijo para que quienquiera que crea en Jesús no será castigado por sus pecados, sino vivirá con Dios para siempre.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-10.jpg}}

Because of your sin, you are guilty and deserve to die. God should be angry with you, but he poured out his anger on Jesus instead of on you. When Jesus died on the cross, he received your punishment.

Por su pecado, Ud. está culpable y merece morir. Dios debería estar enojado con Ud., pero él derramó su enojo en Jesús en vez de en Ud. Cuando Jesús murió en la cruz, él recibió su castigo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-11.jpg}}

Jesus never sinned, but he chose to be punished and to die as the perfect sacrifice to take away your sins and the sins of every person in the world. Because Jesus sacrificed himself, God can forgive any sin, even terrible sins.

Jesús nunca pecó , pero él escogió ser castigado y matado como el sacrificio perfecto para quitar sus pecados y los pecados de toda persona en el mundo. Porque Jesús se sacrificó a sí mismo, Dios puede perdonar cualquier pecado, aun los pecados más terribles.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-12.jpg}}

Good works cannot save you. There is nothing you can do to have a relationship with God. Only Jesus can wash away your sins. You must believe that Jesus is the Son of God, that he died on the cross instead of you, and that God raised him to life again.

Las buenas obras no pueden salvarle a Ud. No hay nada que Ud. puede hacer para para tener una relación con Dios. Solamente Jesús puede limpiar sus pecados. Ud. debe creer que Jesús es el Hijo de Dios, que él murió en la cruz en vez de Ud., y que Dios le levantó a la vida otra vez.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-13.jpg}}

God will save everyone who believes in Jesus and receives him as their Master. But he will not save anyone who does not believe in him. It does not matter if you are rich or poor, man or woman, old or young, or where you live. God loves you and wants you to believe in Jesus so he can have a close relationship with you.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-14.jpg}}

Jesus invites you to believe in him and to be baptized. Do you believe that Jesus is the Messiah, the only Son of God? Do you believe that you are a sinner and that you deserve for God to punish you? Do you believe that Jesus died on the cross to take away your sins?

Jesús nos invita que creamos en él y que seamos bautizados. ¿Cree Ud. que Jesús es el Mesías, el único Hijo de Dios? ¿Cree Ud. que es un pecador y que merece ser castigado por Dios? ¿Cree que Jesús murió en la cruz para quitarle sus pecados?

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-15.jpg}}

If you believe in Jesus and what he has done for you, you are a Christian! God has taken you out of Satan's kingdom of darkness and put you into God's kingdom of light. God has taken away your old, sinful ways of doing things and has given you new, righteous ways of doing things.

¡Si cree en Jesús y lo que ha hecho por Ud., Ud. es un Cristiano! Dios le ha quitado del reino de oscuridad de Satanás y le ha colocado en el reino de luz de Dios. Dios ha quitado su manera vieja pecaminosa de hacer las cosas y le ha dado una manera nueva y justa de hacer las cosas.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-16.jpg}}

If you are a Christian, God has forgiven your sins because of what Jesus did. Now, God considers you to be a close friend instead of an enemy.

Si Ud. es un Cristiano, Dios le ha perdonado sus pecados por lo que Jesús hizo. Ahiora, Dios le considera como un amigo cercano en vez de un enemigo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-17.jpg}}

If you are a friend of God and a servant of Jesus the Master, you will want to obey what Jesus teaches you. Even though you are a Christian, you will still be tempted to sin. But God is faithful and says that if you confess your sins, he will forgive you. He will give you strength to fight against sin.

Si es un amigo de Dios y un siervo de Jesús el Señor, va a querer obedecer lo que Jesús le enseña. Aunque es un Cristiano, todavía será tentado a pecar. Pero Dios es fiel y dice que si confiesa sus pecaods, él le perdonará. El le dará las fuerzas para pelelear contra el pecado.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-18.jpg}}

God tells you to pray, to study his word, to worship him with other Christians, and to tell others what he has done for you. All of these things help you to have a deeper relationship with him.

Dios le dice que ore, estudie su palabra, adórele con otros Cristianos, y diga a otros lo que ha hecho por Ud. Todas estas cosas le ayudará tener una relación más profunda con él.

//A Bible story from: Romans 3:21-26, 5:1-11; John 3:16; Mark 16:16; Colossians 1:13-14; 2 Corinthians 5:17-21; 1 John 1:5-10//

//Una historia de la Biblia de: Romanos 3:21-26, 5:1-11; Juan 3:16; Marcos 16:16; Colosenses 1:13-14; 2 Coríntios 5:17-21; 1 Juan 1:5-10//
