====== 36. The Transfiguration ======

====== 36. La Transfiguración ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-01.jpg?direct&}}

One day, Jesus took three of his disciples, Peter, James, and John with him. (The disciple named John was not the same person who baptized Jesus.) They went up on a high mountain by themselves to pray.

Un día, Jesús llevó a tres de sus discípulos, Pedro, Santiago y Juan consigo. (El discípulo nombrado Juan no era la misma persona que bautizó a Jesús.) Ellos fueron a una montaña alta para orar.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-02.jpg?direct&}}

As Jesus was praying, his face became as bright as the sun and his clothes became as white as light, whiter than anyone on earth could make them.

Mientras Jesús oraba, su cara llegó a ser tan brillante come el sol y su ropa llegó a estar tan blanca como la luz, más blanco que cualquier persona en la tierra podría blanquearla.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-03.jpg?direct&}}

Then Moses and the prophet Elijah appeared. These men had lived on the earth hundreds of years before this. They talked with Jesus about his death, which would soon happen in Jerusalem.

Entonces, Moisés y el profeta Elías aparecieron. Estos hombres habían vivido en la tierrra centenares de años antes. Ellos hablaron con Jesús acerca de su muerte, que iba a suceder pronto en Jerusalén.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-04.jpg?direct&}}

As Moses and Elijah were talking with Jesus, Peter said to Jesus, "It is good for us to be here. Let us make three shelters, one for you, one for Moses, and one for Elijah." Peter did not know what he was saying.

Mientras Moisés y Elías estuvieron hablando con Jesús, Pedro dijo a Jesús. "Es bueno que estemos aquí. Déjanos hacer tres enramadas, uno para ti, uno para Moisés, y uno para Elías." Pedro no sabía lo que estaba diciendo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-05.jpg?direct&}}

As Peter was talking, a bright cloud came down and surrounded them and a voice from the cloud said, "This is my Son whom I love. I am pleased with him. Listen to him." The three disciples were terrified and fell to the ground.

Mientras Pedro estaba hablando, una nube brillante bajó y les envolvió y una voz de la nube dijo, "Este es mi Hijo que yo amo. Estoy orgulloso de él. Escúchenle." Los tres discípulos estaban aterrorizados y se cayeron al suelo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-06.jpg?direct&}}

Then Jesus touched them and said, "Do not be afraid. Get up." When they looked around, the only one still there was Jesus.

Entonces, Jesús les tocó y les dijo, "No tengan miedo. Levántense." Cuando ellos miraron alrededor, el único que estaba allí era Jesús.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-07.jpg?direct&}}

Jesus and the three disciples went back down the mountain. Then Jesus said to them, "Do not tell anyone yet about what happened here. I will soon die and then come back to life. After that, you may tell people."

Jesús y los tres discípulos bajaron de la montaña. Entonces, Jesús les dijo, "No digan a nadie todavía acerca de lo que sucedió aquí. Pronto moriré y luego volveré a la vida. Después de esto, pueden decirlo a la gente."

//A Bible story from: Matthew 17:1-9; Mark 9:2-8; Luke 9:28-36//

//Una historia de la Biblia de: Mateo 17:1-9; Marcos:2-8; Lucas 9:28-36//
