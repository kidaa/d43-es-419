====== 4. God's Covenant with Abraham/ El Pacto de Dios con Abraham ======

===== Información General: =====

Sin caused everyone to work together to disobey God. Therefore, God divided them into many different language groups and scattered them over the earth. But he chose Abram to follow him. God promised to make the descendants of Abram into a great nation, and to bless all of the families of the earth through him.

El pecado hizo que todos trabajaran juntos para desobedecer a Dios.  Por lo tanto, Dios los dividió en por diferentes grupos de lenguajes y los esparció sobre la tierra.  Pero Él escogió a Abram para que le siguiera.  Dios prometió convertir a los descendientes de Abram en una gran nación, y bendecir todas las familias de la tierra a través de él.

===== Translate the Story =====

Click on the images below to translate each frame of the story.

[[:es-419:obs:notes:frames:04-01|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-01.jpg?nolink&200}}]]\\
One People [04-01] **Un pueblo** \\
\\
[[:es-419:obs:notes:frames:04-02|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-02.jpg?nolink&200}}]]\\
The Tower [04-02] **La Torre** \\
\\
[[:es-419:obs:notes:frames:04-03|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-03.jpg?nolink&200}}]]\\
Many Languages [04-03] **Muchos Idiomas** \\
\\
[[:es-419:obs:notes:frames:04-04|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-04.jpg?nolink&200}}]]\\
God Chose Abram [04-04] **Dios escogió a Abram** \\
\\
[[:es-419:obs:notes:frames:04-05|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-05.jpg?nolink&200}}]]\\
To Canaan [04-05] **A Canaán** \\
\\
[[:es-419:obs:notes:frames:04-06|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-06.jpg?nolink&200}}]]\\
God Blessed Abram [04-06] **Dios Bendijo a Abram** \\
\\
[[:es-419:obs:notes:frames:04-07|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-07.jpg?nolink&200}}]]\\
Melchizedek [04-07] **Melquisedec** \\
\\
[[:es-419:obs:notes:frames:04-08|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-08.jpg?nolink&200}}]]\\
No Son [04-08]  **No Hijo** \\
\\
[[:es-419:obs:notes:frames:04-09|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-04-09.jpg?nolink&200}}]]\\
God's Covenant [04-09] **El Pacto de Dios** \\
//Una Historia de la Biblia:  Génesis 11-15//

**[[:es-419:obs:notes:03|<< Previous]] | [[:es-419:obs:notes:05|Next >>]]**
