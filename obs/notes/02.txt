====== 2. El Pecado entra en el Mundo ======


===== Información General: =====


God created a perfect world for Adam and Eve, but they were not satisfied. They ate the only fruit that was forbidden. This brought sin and suffering into the world.  They immediately became fearful and ashamed, and were separated from God. Therefore, God removed them from the garden so they would not live forever in that sinful condition.

**Dios creó un mundo perfecto para Adán y Eva, pero ellos no estaban satisfechos.  Ellos comieron de la única fruta que les fue prohibida.  Esto trajo el pecado y el sufrimiento al mundo.  Ellos inmediatamente se avergonzaron y se convirtieron en miedosos, y estaban separados de Dios.  Por otro lado, Dios los sacó del jardín así no vivirían para siempre en esa condición pecaminosa.**


 


[[:es-419:obs:notes:frames:02-01|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-01.jpg?200||Frame 02-01}}]]\\
Un Mundo Perfecto[02-01]\\
\\
[[:es-419:obs:notes:frames:02-02|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-02.jpg?200|Frame 02-02}}]]\\
La serpiente [02-02]\\
\\
[[:es-419:obs:notes:frames:02-03|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-03.jpg?200|Frame 02-03}}]]\\
No tocar [02-03]\\
\\
[[:es-419:obs:notes:frames:02-04|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-04.jpg?200|Frame 02-04}}]]\\
La mentira [02-04]\\
\\
[[:es-419:obs:notes:frames:02-05|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-05.jpg?200|Frame 02-05}}]]\\
Desobediencia [02-05]\\
\\
[[:es-419:obs:notes:frames:02-06|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-06.jpg?200|Frame 02-06}}]]\\
Ojos abiertos [02-06]\\
\\
[[:es-419:obs:notes:frames:02-07|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-07.jpg?200|Frame 02-07}}]]\\
¿Dónde estás? [02-07]\\
\\
[[:es-419:obs:notes:frames:02-08|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-08.jpg?200|Frame 02-08}}]]\\
La culpa [02-08]\\
\\
[[:es-419:obs:notes:frames:02-09|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-09.jpg?200|Frame 02-09}}]]\\
Dios maldice la serpiente [02-09]\\
\\
[[:es-419:obs:notes:frames:02-10|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-10.jpg?200|Frame 02-10}}]]\\
Dios maldice la mujer [02-10]\\
\\
[[:es-419:obs:notes:frames:02-11|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-11.jpg?200|Frame 02-11}}]]\\
Dios maldice al hombre [02-11]\\
\\
[[:es-419:obs:notes:frames:02-12|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-12.jpg?200|Frame 02-12}}]]\\
Fuera del jardín [02-12]\\
\\

//Una Historia Bíblica de: Genesis 3//

**[[:es-419:obs:notes:01|<< Previous]] | [[:es-419:obs:notes:03|Next >>]]**
