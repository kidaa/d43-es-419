===== Rehoboam's Foolish Answer [18-06] Respuesta tonta de Roboam [18-06] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-18-06.jpg?nolink&}}

**Rehoboam foolishly answered** them, “You thought my father **Solomon** made you work hard, but I will make you work harder than he did, and I will **punish** you more harshly than he did.”

**Roboam** tontamente les respondió: “Usted pensó que mi padre** Salomón** hizo usted trabajar duro, pero me hará trabajar más duro de lo que hizo, y yo los** castigaré** con más dureza de lo que él hizo.”
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:rehoboam|Rehoboam]]**
  * **[[:es-419:obs:notes:key-terms:solomon|Solomon]]**
  * **[[:es-419:obs:notes:key-terms:punish|punish]]**
==== Translation Notes: ====

  * **foolishly answered**  - Rehoboam's answer was harsh, and caused the people to turn against him.
  * **tontamente contesto -**  La repuesta de Rehoboam fue violenta y causo que la gente se fueran en contra de el.
**[[:es-419:obs:notes:frames:18-05|<<]] | [[:es-419:obs:notes:18|Up]] | [[:es-419:obs:notes:frames:18-07|>>]]**
