===== Joseph Stored Up Food [08-09] / Jose almaceno comida [08-09] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-09.jpg?nolink&}}

**Joseph** told the people to **store up large amounts of food** during the seven years of good harvests. Then Joseph sold the food to the people when the seven years of **famine** came so they would have enough to eat.

**José** dijo a la gente que almacene cantidades grandes de comida durante los siete años de las cosechas buenas. Entonces, **José** vendía la comida a la gente cuando los siete años de hambre llegaron para que tuvieran lo suficiente para comer.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:joseph|Joseph]]**
==== Translation Notes: ====

  * **store up large amounts of food**  - They took food from the abundant harvests to the cities and stored it there. The food then belonged to Pharaoh.
  * **almacenar cantidades grandes de comida** - Ellos tomaron comida de cosecha abundante a las cuidades y la almacenaron. La comida le pertenecia a Faraon
  * **famine**  - See how you translated this in **[[:es-419:obs:notes:frames:08-07|[08-07]]]**.
  * **hambruna** - escacez de alimentos **[[:es-419:obs:notes:frames:08-07|[08-07]]]**.
**[[:es-419:obs:notes:frames:08-08|<<]] | [[:es-419:obs:notes:08|Up]] | [[:es-419:obs:notes:frames:08-10|>>]]**
