===== Firstborn Males Would Die [11-01] / Varones Primogenitos Moririan [11-01] F =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-11-01.jpg?nolink&}}

**God** warned **Pharaoh** that if he did not let the **Israelites** go, then he would kill all the **firstborn males of both people and animals**. When Pharaoh heard this he still refused to **believe** and **obey** God.

**Dios ** advirtió al** Faraón** que si no dejaba ir a los **Israelitas**, entonces, él iba a matar a todos los varones primogénitos de las personas y los animales machos. Cuando el **Faraón ** oyó esto, todavía se **negaba a creer ** y **obedecer a Dios.**
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God (Dios)]]  **
  * **[[:es-419:obs:notes:key-terms:pharaoh|Pharaoh (Faraon)]] **
  * **[[:es-419:obs:notes:key-terms:israel|Israelites (Israelitas)]] **
  * **[[:es-419:obs:notes:key-terms:believe|believe (creer) ]]**
  * **[[:es-419:obs:notes:key-terms:obey|obey (obedecer)]] **
==== Translation Notes: ====

  * **the firstborn males of both people and animals**  - This could be translated as, "the eldest son in every family and the first male offspring of any of their animals."
  * **los primogénitos varones de personas y animales - **  Esto puede ser traducido como, "el hijo mayor en cada familia y el primer macho de cualquiera de sus animales."
**[[:es-419:obs:notes:frames:10-12|<<]] | [[:es-419:obs:notes:11|Up]] | [[:es-419:obs:notes:frames:11-02|>>]]**
