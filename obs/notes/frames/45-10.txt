===== Philip Preached about Jesus [45-10] / Felipe Predico sobre Jesus =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-45-10.jpg?nolink&}}

Philip explained to the Ethiopian man that **Isaiah** was writing about **Jesus**. Philip also used other **scriptures** to tell him the **good news** about Jesus.

Felipe explicó al Etíope que Isaías estuvo escribiendo acerca de Jesús. Felipe también, usó otras escrituras para contarle las buenas nuevas acerca de Jesús.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:isaiah|Isaiah]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:word-of-god|scriptures]]**
  * **[[:es-419:obs:notes:key-terms:good-news|good news]]**
===== Translation Notes: =====

  * //(There are no notes for this frame.)//
  * **(No hay**  no**tas**** para este marco.)**
**[[:es-419:obs:notes:frames:45-09|<<]] | [[:es-419:obs:notes:45|Up]] | [[:es-419:obs:notes:frames:45-11|>>]]**
