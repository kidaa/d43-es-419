===== God Will Provide/Dios Proveerá [05-07] *F =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-05-07.jpg?nolink&}}

As **Abraham** and **Isaac walked to the place of sacrifice**, Isaac asked, “Father, we have **wood for the sacrifice**, but where is **the lamb**?” Abraham replied, “**God** will provide the lamb for the sacrifice, my son.”

Mientras Abraham e Isaac caminaron al lugar del sacrificio, Isaac preguntó, “¿Padre, tenemos la madera para el sacrificio, pero dónde está el cordero? Abraham respondio, "Dios proveera el cordero para el sacrificio, mi hijo,"

===== Important Terms: Términos Importantes =====

  * **[[:es-419:obs:notes:key-terms:abraham|Abraham (Abraham)]] **
  * **[[:es-419:obs:notes:key-terms:isaac|Isaac (Isaac)]]**
  * **[[:es-419:obs:notes:key-terms:sacrifice|sacrifice (sacrificio)]]**
  * **[[:es-419:obs:notes:key-terms:lamb|lamb (cordero)]]**
  * **[[:es-419:obs:notes:key-terms:god|God (Dios)]]**
===== Translation Notes: Notas de Traducción =====

  * **walked to the place of sacrifice**  - God had told Abraham to sacrifice Isaac on a special high hill that was about three days' walk from where they lived.
  * **caminando al lugar del sacrificio**  - Dios pidió a Abraham que sacrificara a Isaác en una colina especial que estaba a unos tres días de camino de donde ellos vivían.
  * **wood for the sacrifice**  - For a sacrifice, normally the lamb was killed and then placed on top of wood so that the wood and the lamb could be burned up with fire.
  * **la leña para el sacrificio**  - Para hacer un sacrificio, normalmente el cordero se colocaba sobre la leña para que la leña y el cordero se quemaran al mismo tiempo.
  * **the lamb**  - A young sheep or goat would be the normal animal for a sacrifice.
  * **el cordero**  - Una oveja jóven o cabra sería el animal más popular para sacrificar.
  * **provide**  - Abraham may have believed that Isaac was the "lamb" that God provided, although God fulfilled Abraham's words by providing a ram to sacrifice in Isaac's place.
  * **proveer**  - Abraham pudo haber pensado que Isaác era "el cordero" que Dios había provisto, pero Dios cumplió con las palabras dichas por Abraham proveyendo una cabra para el sacrificio en vez de Isaác.
**[[:es-419:obs:notes:frames:05-06|<<]] | [[:es-419:obs:notes:05|Up]] | [[:es-419:obs:notes:frames:05-08|>>]]**
