===== Pharaoh's Hard Heart [10-09] / El Corazon Endurecido Faraon [10-09] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-10-09.jpg?nolink&}}

But **Pharaoh sinned** again and **hardened his heart**. He would not let the **Israelites** go free.

Pero el **Faraón** pecó otra vez y endureció su corazón. No dejaría que los **Israelitas** se fueran libres.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:pharaoh|Pharaoh]]**
  * **[[:es-419:obs:notes:key-terms:sin|sinned]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
==== Translation Notes: ====

  * **hardened his heart**  - See note in **[[:es-419:obs:notes:frames:10-04|[10-04]]]**.
**[[:es-419:obs:notes:frames:10-08|<<]] | [[:es-419:obs:notes:10|Up]] | [[:es-419:obs:notes:frames:10-10|>>]]**
