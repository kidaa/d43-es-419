===== Let My People Go! [10-01] / Deja que mi gente se vaya [10-01] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-10-01.jpg?nolink&}}

**Moses** and **Aaron** went **to Pharaoh**. They said, “This is what the **God of Israel** says, ‘**Let my people go**!’” Pharaoh did not **listen to** them. Instead of letting the **Israelites** go free, he forced them to work even harder!

**Moisés** y **Aarón ** fueron a** Faraón**. Ellos dijeron: “Esto es lo que el **Dios** de** Israel ** dice: '¡Deje que mi gente vaya!'” **Faraón** no los escuchó. En vez de dejar ir a los **Israelitas** en libertad, los obligó que trabajaran aún más duro!
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:moses|Moses]]**
  * **[[:es-419:obs:notes:key-terms:aaron|Aaron]]**
  * **[[:es-419:obs:notes:key-terms:pharaoh|Pharaoh]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israel]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
==== Translation Notes: ====

  * **to Pharaoh**  – It may be clearer to say, "to Pharaoh's palace to talk to him."
  * **a Faraón: **  Podría ser mas claro decir: "al palacio del Faraón para hablar con él."
  * **God of Israel**  - This could be translated as, "God, who chose the Israelites to be his people" or, "God, who rules the people of Israel" or, "the God whom the Israelites worship."
  * **Dios de Israel:**  Esto tambien puede traducirse como: "Dios, que escogio a los Israelitas para que fueran su pueblo", o "Dios, aquel que rige a Israel", o "El Dios a quien los Israelitas adoran".
  * **Let My people go**  – Another way to say this would be, "Allow My people to go free" or, "Free My people to leave Egypt."
  * **deja salir a mi pueblo: **  Otra manera de decir esto es: "Deja a Mi pueblo salir libre de aqui", o "Libera a mi pueblo para que salgan de Egipto".
  * **my people**  - See "my people" in **[[:es-419:obs:notes:frames:09-13|[09-13]]]**.
  * **Mi pueblo** - Ver "my pueblo" en **[[:es-419:obs:notes:frames:09-13|[09-13]]]**.
  * **listen to**  – This could be translated as "heed" or, "obey."
  * **escuchar a: **  Esto puede traducirse como "no les puso atencion", o "estar de acuerdo" . 
__**NOTA:**__ Faraon, siendo el Dios RA encarnado, no podia "obedecer" a un mortal, especialmente Israelita.

**[[:es-419:obs:notes:frames:09-15|<<]] | [[:es-419:obs:notes:10|Up]] | [[:es-419:obs:notes:frames:10-02|>>]]**
