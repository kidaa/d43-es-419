===== God's Word Gives Life [25-03] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-25-03.jpg?nolink&}}

**Jesus** replied, “It is written in **God’s word**, ‘People do not only need **bread** in order to live, **but they need every word that God speaks**!’”

Jesús le respondió: “Está escrito en la palabra de Dios,” La gente no sólo necesita pan para vivir, pero necesitan cada palabra que Dios habla! '”

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms/jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms/word-of-god|God's word]]**
  * **[[:es-419:obs:notes:key-terms/god|God]]**


===== Translation Notes: =====

  * **bread**  - Make sure that you use the same word to translate "bread" here as you did in **[[:es-419:obs:notes:frames:25-02|[25-02]]]**.
  * **pan **- asegurese que usted use la misma palabra traducida "pan" como fue usada [25 - 02]
  * **but they need every word that God speaks!**  - Another way to translate this would be, "Rather, people need to listen to and obey everything God says" or, "Rather, people have real life by believing and obeying what God says."
  * **ellos necesitan cada palabra que viene de Dios!**  - otra manera de traducir esto es, "preferir", la gente necesita escuchar y obedecer todo lo que Dios dice", o "perferir", gente que tiene una vida real creyendo y obedeciendo lo que Dios dice".
**[[:es-419:obs:notes:frames:25-02|<<]] | [[:es-419:obs:notes:25|Up]] | [[:es-419:obs:notes:frames:25-04|>>]] **
