===== Israel Helped the Gibeonites / Israel ayudo a los Gibeonitas[15-08] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-15-08.jpg?nolink&}}

So **Joshua** gathered the **Israelite** army and they marched all night **to reach the Gibeonites**. In the early morning **they surprised the Amorite armies** and attacked them.

Así**, Josué** reunió el ejército **Israelita​ ** y marcharon toda la noche para llegar dónde los **Gibeonitas**. En la madrugada ellos soprendieron los ejercitos de los **Amorreos** y los atacaron.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:joshua|Joshua]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israelite]]**
  * **[[:es-419:obs:notes:key-terms:gibeon|Gibeonites]]**
  * **[[:es-419:obs:notes:key-terms:amorite|Amorite]]**
==== Translation Notes: ====

  * **to reach the Gibeonites**  - That is, "to get to the Gibeonites" or, "to arrive at where the Gibeonites lived." The Gibeonites lived in Canaan, but Canaan is big enough that it took all night for the Israelite army to travel from their camp to where the Gibeonites were.
  * **para alcansar los Gibeonitas** - Eso es, "para llegar a los Gibeonitas" o, "para llegar a donde los Gibeonitas vivian." Los Gibeonitas vivian en Canaan, pero Canaan esta lo suficiente grande que se tomo toda la noche para el ejército Israelita para viajar desde su campamento hasta donde los Gibeonitas estaban.
  * **they surprised the Amorite armies**  - The Amorites did not know that the Israelites were coming to attack them.
  * **ellos sorprendieron los ejercitos de los Amorreos** - Los Amorreos no sabian que los Israelitas venian para atacarlos. 
**[[:es-419:obs:notes:frames:15-07|<<]] | [[:es-419:obs:notes:15|Up]] | [[:es-419:obs:notes:frames:15-09|>>]]**
