===== Killing the Prophets of Baal [19-12]/Matando los Profetas de Baal [19-12] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-19-12.jpg?nolink&}}

Then **Elijah** said, “Do not let any of the **prophets** of **Baal escape**!” So the people **captured** the prophets of Baal. Then Elijah took them away from there and killed them.

Entonces, Elías le dijo: “No dejen que ninguno de los profetas de Baal escapen!” Así que las personas capturadas a los profetas de Baal y se los llevaron de allí y los mataron.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:elijah|Elijah]]**
  * **[[:es-419:obs:notes:key-terms:prophet|prophets]]**
  * **[[:es-419:obs:notes:key-terms:baal|Baal]]**
==== Translation Notes: ====

  * **escape**  - The prophets of Baal tried to run away after their god was proven to be a false god.
  * **Escape - ** Los profetas de Baal trataron de escaparse despues que su dios provo ser un dios falso.
  * **captured**  – That is, "seized and held onto" or, "took hold of."
  * **capturado - ** Esto es, "agarrados y retenidos" o, se apoderaron,"
  *

**[[:es-419:obs:notes:frames:19-11|<<]] | [[:es-419:obs:notes:19|Up]] | [[:es-419:obs:notes:frames:19-13|>>]]**
