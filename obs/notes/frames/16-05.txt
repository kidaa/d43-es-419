===== An Angel Spoke to Gideon / El angel le hablo a Gedeón [16-05] *B =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-16-05.jpg?nolink&}}

One day, a man of Israel named **Gideon** was **threshing grain secretly** so the **Midianites** would not steal it. The **angel** of **Yahweh** came to Gideon and said, “**God**** is with you**, mighty warrior. Go and save **Israel** from the Midianites.”

Un día, un hombre de **Israel** llamado **Gedeón** estaba trillando el grano en secreto para que los **madianitas** no pudieran robarlo. El **ángel** del **Señor** vino a **Gedeón** y le dijo: “**Dios** está contigo, varón esforzado y valiente. Ve y** salva ** a **Israel** de los **madianitas**.”
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:gideon|Gideon]]**
  * **[[:es-419:obs:notes:key-terms:midian|Midianites]]**
  * **[[:es-419:obs:notes:key-terms:angel|angel]]**
  * **[[:es-419:obs:notes:key-terms:yahweh|Yahweh]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israel]]**
==== Translation Notes: ====

  * **One day**  - This phrase introduces an event that happened in the past, but does not state the specific time. Many languages have a similar way to begin telling a true story.
  * **Un dia**  ​- Esta frase introduce un evento que sucedió en el pasado, pero no establece un tiempo especifico. Muchos idiomas tiene una forma similar para comenzar a relatar una historia real.
  * **threshing grain**  - The grain was wheat, which has a head of many small grains, or seeds, on the top of a thin stalk. "Threshing" is separating the seeds of the plant from the stalks by beating the heads of grain. The seeds are food, but the stalks are not.
  * **estrillando grano**- El grano era el trigo, sobre el tope de la delgada espiga, tenia una cabeza de muchos granos o semillas pequeñas, ."Estrillar" consiste es la separación de las semillas o granos de las delgadas espigas por sacudimiento. Las semillas son alimentos, pero las espigas no lo son.
  * **secretly**  - Gideon was threshing the grain in a hidden location, so the Midianites would not see him.
  * **secretamente**- Gedeon estaba estrillando el grano en un lugar escondido, asi que los Medianitas no lo verían a el.
  * **God is with you**  – This means, "God is present with you in a special way" or, "God has plans to use you in a special way."
  * **Dios esta contigo**- Esto significa, "Dios esta presente contigo en una forma especial" o "Dios tiene planes para usarte a ti en una forma especial".
  *

**[[:es-419:obs:notes:frames:16-04|<<]] | [[:es-419:obs:notes:16|Up]] | [[:es-419:obs:notes:frames:16-06|>>]]**
