===== The Painful Sores [10-07] / Las Llagas Dolorosas [10-07] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-10-07.jpg?nolink&}}

Then **God** told **Moses** to throw ashes into the air in front of **Pharaoh**. When he did, painful skin sores appeared on the **Egyptians**, but not on the **Israelites**. **God hardened Pharaoh's heart**, and Pharaoh would not let the Israelites go free.

Entonces, **Dios ** le dijo a **Moisés ** que tirara cenizas en el aire delante del **Faraón**. Cuando lo hizo, llagas dolorosas en la piel aparecieron en los** Egipcios**, pero no sobre los** Israelitas**. **Dios ** endureció el corazón del **Faraón**, y el **Faraón ** no dejaría que los I**sraelitas** se fueran libres.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:moses|Moses]]**
  * **[[:es-419:obs:notes:key-terms:pharaoh|Pharaoh]]**
  * **[[:es-419:obs:notes:key-terms:egypt|Egyptians]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
==== Translation Notes: ====

  * **God hardened Pharaoh's heart**  - God caused Pharaoh to continue to be stubborn. See also note in **[[:es-419:obs:notes:frames:10-04|[10-04]]]**.
  * **Dios endureció el corazón del Faraón** - Dios causó que el corazón del Faraón continuara endureciéndose. Ver nota en **[[:es-419:obs:notes:frames:10-04|[10-04]]]**.
**[[:es-419:obs:notes:frames:10-06|<<]] | [[:es-419:obs:notes:10|Up]] | [[:es-419:obs:notes:frames:10-08|>>]]**
