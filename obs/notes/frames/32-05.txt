===== Jesus Commanded the Demons [32-05] =====

<font 20px/inherit;;inherit;;inherit>** Jesús Ordenó a los  **</font>    ** <font 20px/inherit;;inherit;;inherit>Demonios [32-05]</font>    **

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-32-05.jpg?nolink&}}

When the man came to **Jesus**, he **fell on his knees** in front of him. Jesus said to the **demon**, “Come out of this man!”

Cuando el hombre llegó a **Jesús**, se cayó de rodillas ante él. Jesús dijo al **demonio**, “Salga de este hombre!”
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:demon|demon]]**
===== Translation Notes: =====

  * **fell on his knees**  - That is, "quickly knelt down on the ground."
  * **cayó de rodillas **- Esto es, ' rapidamente se arrodillo sobre la tierra."
**[[:es-419:obs:notes:frames:32-04|<<]] | [[:es-419:obs:notes:32|Up]] | [[:es-419:obs:notes:frames:32-06|>>]]**
