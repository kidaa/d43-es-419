===== The Messiah Will Bring Peace [21-13] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-21-13.jpg?nolink&}}

The **prophets** also said that the **Messiah** would be perfect, **having no sin**. He would **die** to **receive the punishment for other people’s sin**. His punishment would bring **peace** between **God** and people. For this reason, **it was God's will to crush the Messiah**.

Los profetas también, dijeron que el Mesías sería perfecto, sin tener pecado. Moriría para recibir el castigo por el pecado de los demás. Su castigo traería la paz entre Dios y la gente. Por esta razón, era la voluntad de Dios para aplastar al Mesías.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:prophet|prophets]]**
  * **[[:es-419:obs:notes:key-terms:messiah|Messiah]]**
  * **[[:es-419:obs:notes:key-terms:sin|sin]]**
  * **[[:es-419:obs:notes:key-terms:death|die]]**
  * **[[:es-419:obs:notes:key-terms:receive|receive]]**
  * **[[:es-419:obs:notes:key-terms:punish|punishment]]**
  * **[[:es-419:obs:notes:key-terms:peace|peace]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
==== Translation Notes: ====

  * **having no sin**  - This can also be translated as "he never sinned."
  * **no tenia pecado** - Esto puede ser traducido como " el nunca peco."
  * **receive the punishment for other people's sin**  - That is, "to take on himself the punishment that other people deserved" or, "to be punished in the place of other people."
  * **recivio el castigo por el pecado de otra gente**  - Esto es, "tomar sobre si mismo el castigo de otras personas que no se lo merecian" o, "ser castigado por otra gente." 
  * **it was God's will**  - That is, "it fulfilled God's purpose." This phrase means that the Messiah's death was exactly what God had planned to happen in order that his sacrifice would pay for the sins of the people.
  * **fue el deseo de Dios** - Esto es, "El proposito  fue cumplido." Esta frase  significa  que la  muerte del  Mesias era exactamente lo que Dios planifico  que pasara en orden que su sacrificio  pagara por los pecados de la gente.
  * **to crush**  - That is, "to damage completely," "to kill" or, "to completely destroy."
  * **para aplastar** - Esto es, " hacer dano completamente , o, "matar" o, completamente destruir."
**[[:es-419:obs:notes:frames:21-12|<<]] | [[:es-419:obs:notes:21|Up]] | [[:es-419:obs:notes:frames:21-14|>>]]**
