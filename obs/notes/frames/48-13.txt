===== A Descendent of David [48-13] / Un Descendiente de David =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-48-13.jpg?nolink&}}

**God promised King David** that one of his **descendants** would rule as **king** over God’s people forever. Because **Jesus** is the **Son of God** and the **Messiah**, is that special descendant of David who can rule forever.

**Dios** **prometió al rey David ** que uno de sus** decendientes ** reinaría como** rey ** sobre la gente de Dios para siempre. Porque **Jesús** es el **Hijo de Dios ** y el **Mesías**, él es aquel decendiente especial de David que puede reinar para siempre.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:promise|promised]]**
  * **[[:es-419:obs:notes:key-terms:david|King David]]**
  * **[[:es-419:obs:notes:key-terms:god-son|Son of God]]**
  * **[[:es-419:obs:notes:key-terms:messiah|Messiah]]**
  * **[[:es-419:obs:notes:key-terms:descendant|descendants]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
===== Translation Notes: =====

  * //(There are no notes for this frame.)//
  * //(No hay notas para este marco.)//
**[[:es-419:obs:notes:frames:48-12|<<]] | [[:es-419:obs:notes:48|Up]] | [[:es-419:obs:notes:frames:48-14|>>]]**
