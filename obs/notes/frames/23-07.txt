===== The Angels Praised God [23-07] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-23-07.jpg?nolink&}}

“Go search for the baby, and you will find him **wrapped in pieces of cloth** and lying in a feeding trough.” Suddenly, the skies were **filled with angels** praising God, saying, “**Glory to God** in **heaven** and **peace on earth** to **the people he favors**!”

“Vayan para buscar al bebé, y ustedes lo encontrarán envuelto en trozos de tela y acostado en un pesebre.” De repente, los cielos se llenaron de ángeles que alababan a Dios, diciendo: “¡Gloria a Dios en el cielo y paz en la tierra a la gente que él favorece!”

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:angel|angels]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:glory|Glory]]**
  * **[[:es-419:obs:notes:key-terms:heaven|heaven]]**
  * **[[:es-419:obs:notes:key-terms:peace|peace]]**
===== Translation Notes: =====

  * (The angel continued speaking.)
  * (El Angel siguio hablando.) 
  * **wrapped in pieces of cloth**  - The custom of that time was to tightly wrap newborn babies in long strips of cloth. It may be necessary to say, "wrapped in long strips of cloth, in the customary way."
  * **envuelto en trozos de tela**- en la forma que se acostumbraba. La costumbre a ese tiempo era envolver apretadamente a los recien nacidos en largas tiras de tela. Puede ser necesario decir , "envueltos en tiras largas de tela. la forma que se acostumbraba."
  * **feeding trough**  - That is, "animal feeding box." Also see how you translated this in **[[:es-419:obs:notes:frames:23-05|[23-05]]]**.
  * **comedero **- Esto es, "el cuadro  animales comen." *Mira tambien como se traduce esto en [23-05]
  * **filled with angels**  - This means that there were so many angels that they seemed to fill the sky
  * **lleno de angeles** - Esto significa que habian muchos angeles que parecian llenar el cielo.
  * **Glory to God**  - This can also be translated as, "Let us all give glory to God!" or, "Our God deserves all glory and honor!" or, "We all give glory to God!"
  * **Gloria a Dios** - Esto puede ser traducido como, "Vamos todos a dar gloria a Dios!" o, "Nuestro Dios marece toda la gloria y honor!" o, "Todos le damos gloria a Dios!"
  * **peace on earth**  - Another way to say this would be, "may there be peace on earth."
  * **paz en la tierra** - Otra forma de decir esto puede ser, "puede haber paz en la tierra."
  * **the people he favors**  - This might be translated as, "people that God looks upon with favor, delight, or good will."
  * **la gente que favorece** - Esto puede ser traducido como, "gente que Dios mira con favor, deleite, or buena voluntad."
 

**[[:es-419:obs:notes:frames:23-06|<<]] | [[:es-419:obs:notes:23|Up]] | [[:es-419:obs:notes:frames:23-08|>>]]**
