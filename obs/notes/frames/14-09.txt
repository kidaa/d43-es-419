===== The People Ignored God / La gente ignoro a Dios [14-09] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-09.jpg?nolink&}}

When the people heard this, they were sorry **they had sinned**. They took their weapons and went to attack the people of **Canaan**. **Moses warned them not to go** because **God was not with them**, **but they did not listen to him**.

Cuando la gente oyó esto, estaban tristes que habían **pecado**. Tomaron sus armas y fueron para atacar a la gente de **Canaán**. **Moises** les avisó que no fueran porque **Dios ** no estaba con ellos, pero ellos no le escucharon.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:sin|sinned]]**
  * **[[:es-419:obs:notes:key-terms:canaan|Canaan]]**
  * **[[:es-419:obs:notes:key-terms:moses|Moses]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
==== Translation Notes: ====

  * **they had sinned**  - It may be necessary to add, "they had sinned by disobeying God's command to conquer the peoples of Canaan."
  * **ellos pecaron **- puede ser necesario agregar "ellos pecaron por desobedecer la orden de Dios para conquistar a la gente de Canaan."
  * **Moses warned them not to go**  - This means that Moses told them not to go to fight against the Canaanites because they would be in danger if they did that.
  * **Moise les advirtio que no fueran **-Esto quiere decir que Moises les dijo no pelear en contra de los Cananeos porque ellos estan en peligro de hacer eso.
  * **God was not with them**  – In other words, God would not be with them to help them. Because of the Israelites' disobedience, God withdrew from them his presence, protection, and power.
  * **Dios no estaba con ellos **- En otras palabras, Dios no estara con ellos para alludarlos. porque los Israelitas desovedecieron, Dios se movio su precensia, proteccion y poder.
  * **but they did not listen to him**  - They did not obey Moses. They went to attack the Canaanites anyway.
  * **ellos no lo escucharon **- ellos no obedecian a Moises. ellos fueron a atacar a los Cananeos de todas maneras.
**[[:es-419:obs:notes:frames:14-08|<<]] | [[:es-419:obs:notes:14|Up]] | [[:es-419:obs:notes:frames:14-10|>>]]**
