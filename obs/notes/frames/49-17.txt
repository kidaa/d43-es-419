===== Follow Jesus' Teachings [49-17]Sigué las Instrucciones de Jesus [49-17] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-17.jpg?nolink&}}

If you are a friend of **God** and a **servant** of **Jesus** the **Master**, you will want to **obey** what Jesus teaches you. Even though you are a **Christian**, you will still **be tempted to sin**. But God **is faithful** and says that if you **confess your sins**, he will **forgive** you. **He will give you strength to fight against sin**.

Si es un amigo de Dios y un siervo de Jesús el Señor, va a querer obedecer lo que Jesús le enseña. Aunque es un Cristiano, todavía será tentado a pecar. Pero Dios es fiel y dice que si confiesa sus pecaods, él le perdonará. El le dará las fuerzas para pelelear contra el pecado.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:servant|servant]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:lord|Master]]**
  * **[[:es-419:obs:notes:key-terms:obey|obey]]**
  * **[[:es-419:obs:notes:key-terms:christian|Christian]]**
  * **[[:es-419:obs:notes:key-terms:tempt|tempted]]**
  * **[[:es-419:obs:notes:key-terms:sin|sin]]**
  * **[[:es-419:obs:notes:key-terms:faithful|faithful]]**
  * **[[:es-419:obs:notes:key-terms:forgive|forgive]]**
===== Translation Notes: =====

  * **be tempted to sin**  - That is, ""be tempted to sin even though you know sin is wrong."
  * **tentacion de pecar **-Esto es "tener tentacion de pecar sabiendo que pecar es mal"
  * **is faithful**  - In this context this means that God, "keeps his promises."
  * **ser fiel **- En este contenido se refiere de que Dios, "cumple sus promesas"
  * **confess your sins**  - This could be translated as, "admit to God what you have done wrong."
  * **confesar tus pecados **- Esto puede ser traducido como, "admitir a Dios que tu has hecho algo mal"
  * **He will give you strength to fight against sin**  - That is, "He will give you spiritual strength to refuse to sin."
  * **El te da fuerza para pelear en contra del pecado**  -Esto es, " El te da fuerza espiritual para rechazar el pecado"
**[[:es-419:obs:notes:frames:49-16|<<]] | [[:es-419:obs:notes:49|Up]] | [[:es-419:obs:notes:frames:49-18|>>]]**
