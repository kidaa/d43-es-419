===== The Message of the Prophets [19-18]/El mensage de los Profetas [19-18] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-19-18.jpg?nolink&}}

The **prophets continued to speak for God** even though the people hated them. They warned people that God would destroy them if they did not **repent**. They also reminded people **of the promise that God’s Messiah would come**.

Los **profetas ** continuaron hablando por **Dios ** a pesar de que la gente los odiaba. Advirtieron a la población para que Dios les destruyera si no se **arrepentieron**. También, recordaron a la gente de la **promesa ** de que el **Mesías ** de Dios vendría.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:prophet|prophets]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:repent|repent]]**
  * **[[:es-419:obs:notes:key-terms:promise|promise]]**
  * **[[:es-419:obs:notes:key-terms:messiah|Messiah]]**
==== Translation Notes: ====

  * **continued to speak for God**  - That is, "continued to tell the people what God wanted to tell them."
  * **continuaron hablando de Dios -**  Esto es, "continuaron diciendole a la gente que Dios queria decirle a ellos."
  * **of the promise that God's Messiah would come**  – This could be translated as, "that God had promised that his Messiah would come to save his people."
  * **una de las promesas era que Dios el Mesias venia pronto - **  Esto se puede traducir como, "que Dios habia prometido que el Mesias iba a venir a salvar su gente."
  * //A Bible story from//  - These references may be slightly different in some Bible translations.
  * **Una historia de la Biblia de - **  Estas referencias pueden ser un poco diferente en algunas traduciones de la Biblia.
  * //Una historia de la Biblia de: 1 Reyes 16-18; 2 Reyes 5; Jeremías 38//
**[[:es-419:obs:notes:frames:19-17|<<]] | [[:es-419:obs:notes:19|Up]] | [[:es-419:obs:notes:frames:20-01|>>]]**
