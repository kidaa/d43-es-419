===== Judah Served Babylon [20-06]/Judá Sirvio a Babilonia [20-06] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-06.jpg?nolink&}}

About 100 years after the **Assyrians** destroyed the **kingdom of Israel**, God sent **Nebuchadnezzar**, **king** of the **Babylonians**, to attack the **kingdom of Judah**. **Babylon** was a powerful **empire**. The king of Judah agreed **to be Nebuchadnezzar’s servant** and pay him a lot of money every year.

Cerca de 100 años después de los asirios destruyeron el reino de Israel, Dios envió a Nabucodonosor, el rey de los caldeos, para atacar al reino de Judá. Babilonia era un imperio poderoso. El rey de Judá, accedió a ser el siervo de Nabucodonosor y le pagó un montón de dinero cada año.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:assyria|Assyrians]]**
  * **[[:es-419:obs:notes:key-terms:kingdom-of-israel|kingdom of Israel]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:nebuchadnezzar|Nebuchadnezzar]]**
  * **[[:es-419:obs:notes:key-terms:king|king]]**
  * **[[:es-419:obs:notes:key-terms:babylon|Babylonians]]**
  * **[[:es-419:obs:notes:key-terms:kingdom-of-judah|kingdom of Judah]]**
  * **[[:es-419:obs:notes:key-terms:babylon|Babylon]]**
  * **[[:es-419:obs:notes:key-terms:servant|servant]]**
==== Translation Notes: ====

  * **empire**  - See how you translated this in **[[:es-419:obs:notes:frames:20-02|[20-02]]]**.
  * **Imperio**
  * **agreed to be**  – The king of Judah was forced to either serve the Babylonian king or be destroyed.
  * **estuvo de acuerdo - ** El de Juda fue forzado a cualquiera a servir al rey de Babilonia o ser destruido.
  * **to be Nebuchadnezzar's servant**  - This could be translated as, "to govern Judah under Nebuchadnezzar's command."
  * **a ser el sirviente de Nebuchadnezzar's - ** Esto se puede traduciir como, "a governar a Juda bajo el comando de Nebuchadnezzar."
**[[:es-419:obs:notes:frames:20-05|<<]] | [[:es-419:obs:notes:20|Up]] | [[:es-419:obs:notes:frames:20-07|>>]]**
