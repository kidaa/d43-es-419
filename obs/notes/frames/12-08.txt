===== Israel Crossed the Sea [12-08] / Israel Cruzo el Mar [12-08] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-08.jpg?nolink&}}

The **Israelites marched** through the sea on dry ground **with a wall of water on either side of them**.

Los** Israelitas** marchaban por el mar en tierra seca con un muro de agua en cada lado de ellos.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
==== Translation Notes: ====

  * **marched**  - This can be translated as, "walked" or, "went."
  * **marchar**  - Esto puede se transladado como "caminar" o "salir"
  * **with a wall of water on either side of them**  - This could be translated as, "and the water on both sides of them stood up tall and straight like a wall."
  * **una pared de agua de cada lado **- Esto puede ser traducido como, "y el agua en dos lados se paro y se quedo parada como una pared."
**[[:es-419:obs:notes:frames:12-07|<<]] | [[:es-419:obs:notes:12|Up]] | [[:es-419:obs:notes:frames:12-09|>>]]**
