===== Jesus the Teacher [49-03] / Jesus el Maestro [49-03] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-03.jpg?nolink&}}

**Jesus** was also **a great teacher**, and he spoke with authority because he is the **Son of God**. He taught that you need to **love** other people **the same way** you love yourself.

**Jesús ** era también, un gran maestro, y él habló con autoridad porque era el **Hijo de Dios**. El enseñó que debemos **amar** a los demás como a nosotros mismos.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:god-son|Son of God]]**
  * **[[:es-419:obs:notes:key-terms:love|love]]**
===== Translation Notes: =====

  * **a great teacher**  - That is, "a very important teacher" or, "an excellent teacher."
  * **un gran maestro - Esto es, "un maestro bien importante" o, "un excelente maestro."**
  * **the same way **  - That is, "as much as" or, "the same amount that" or, "to the same degree that."
  * **del misma manera - Esto es, "tanto como" o, "la misma cantidad que" o, "en la misma medida que."**
**[[:es-419:obs:notes:frames:49-02|<<]] | [[:es-419:obs:notes:49|Up]] | [[:es-419:obs:notes:frames:49-04|>>]]**
