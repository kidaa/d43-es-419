===== God Cursed the Woman/Dios Maldijo a la Mujer [02-10] *F =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-02-10.jpg?nolink&}}

**God** then said to the woman, “I will make **childbirth very painful** for you. You will desire your husband, and he will rule over you.”

Entonces, Dios dijo a la mujer, "Haré que el parto sea muy doloroso para tí. Tú desearás a tu esposo, y él gobernará sobre tí."

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God (Dios)]] **
==== Translation Notes: ====

  * **childbirth very painful**  - Some languages may need to express this as a verb. You could say, "I will cause you to have much more pain when you give birth to children."
  * **parto muy doloroso - **  En algunos lenguajes puede que sea necesario expresar esto como un verbo. Podemos decir: "Haré que tengas mucho mas dolor cuando des a luz".
**[[:es-419:obs:notes:frames:02-09|<<]] | [[:es-419:obs:notes:02|Up]] | [[:es-419:obs:notes:frames:02-11|>>]]**
