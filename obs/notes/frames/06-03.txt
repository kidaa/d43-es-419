===== Isaac Married Rebekah [06-03] / Isaac se Casó con Rebeca *F =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-03.jpg?nolink&}}

**Rebekah** **agreed** to leave her family and go back with the **servant** to **Isaac's** home. Isaac married her as soon as she arrived.

**Rebeca ** aceptó dejar a su familia y regresar con el **siervo ** de la casa de Isaac. **Isaac ** se casó con ella tan pronto como llegó.
===== Important Terms: Términos importantes =====

  * **[[:es-419:obs:notes:key-terms:rebekah|Rebekah]] (Rebeca)**
  * **[[:es-419:obs:notes:key-terms:servant|servant]] (siervo)**
  * **[[:es-419:obs:notes:key-terms:isaac|Isaac's]] (De Isaac)**
===== Translation Notes: =====

  * **Rebekah agreed**  - Even though Rebekah's parents were arranging her marriage, they did not force her to go marry Isaac.
  * **Rebeca aceptó**- A pesar de que los padres de Rebeca arreglaron su matrimonio, ellos no la forzaron casarse con Isaac.
**[[:es-419:obs:notes:frames:06-02|<<]] | [[:es-419:obs:notes:06|Up]] | [[:es-419:obs:notes:frames:06-04|>>]]**
