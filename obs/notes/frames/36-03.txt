===== Moses and Elijah [36-03] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-36-03.jpg?nolink&}}

Then **Moses** and the prophet **Elijah appeared**. These men had lived on the earth hundreds of years before this. They talked with **Jesus** about **his death**, **which would soon happen in Jerusalem**.

Entonces, Moises y el profeta Elías aparecieron. Estos hombres habían vivido en la tierrra centenares de años antes. Ellos hablaron con Jesús acerca de su muerte, que iba a suceder pronto en Jerusalen.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:moses|Moses]]**
  * **[[:es-419:obs:notes:key-terms:prophet|prophet]]**
  * **[[:es-419:obs:notes:key-terms:elijah|Elijah]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:death|death]]**
  * **[[:es-419:obs:notes:key-terms:jerusalem|Jerusalem]]**
===== Translation Notes: =====

  * **appeared**  - It is also possible to say, "appeared out of nowhere." They suddenly were there.
  * **aparecieron** - Esto es tambien es posible decir, "aparecieron de la nada."Ellos de repente estaban ahi.
  * **his death, which would soon happen**  - This could be translated as, "how he would soon die" or, "how he would soon be killed."
  * **su muerte, que pronto iba a suceder** - Esto se puede traducir como, "como el pronto ****iba a morir" o, "como a el pronto lo iban a matar.
  * **in Jerusalem**  - Some languages may prefer to say, "in the city of Jerusalem."
  * **en Jerusalen** - Algunos idiomas prefieren decir, "en la ciudad de Jerusalem."
**[[:es-419:obs:notes:frames:36-02|<<]] | [[:es-419:obs:notes:36|Up]] | [[:es-419:obs:notes:frames:36-04|>>]]**
