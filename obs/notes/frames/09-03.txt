===== Hard Labor [09-03]  Labor Pesada [09-03] =====

 

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-09-03.jpg?nolink&}}

The **Egyptians** forced the **Israelites** to build many buildings and even whole cities. The hard work made their lives **miserable**, but **God blessed them**, and they had even more children.

Los **Egipcios** forzaban a los **Israelitas** para que construyeran muchos edificios y aun ciudades enteras. El trabajo pesado hizo sus vidas miserables, pero **Dios ** les** bendijo**, y ellos tuvieron aun más hijos.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:egypt|Egyptians]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:bless|blessed]]**
==== Translation Notes: ====

  * **miserable**  – This means that they suffered terribly because of how harshly they were treated and how hard they were forced to work. They also felt very discouraged.
  * **miserable**  - Esto significa que ellos sufrieron terriblemente debido a como ellos fueron tratados severamente y como ellos fueron forzado al trabajo pesado.
  * **God blessed them**  - God took care of them, helping them endure the severe treatment and even caused them to become more numerous by giving them children.
  * **Dios les bendijo a ellos**  - Dios tomo cuidado de ellos, ayudándoles a soportar el tratamiento severo y causo incluso que ellos se volvieran mas numerosos por dar ellos hijos.
**[[:es-419:obs:notes:frames:09-02|<<]] | [[:es-419:obs:notes:09|Up]] | [[:es-419:obs:notes:frames:09-04|>>]]**
