===== Moses Killed an Egyptian [09-09]  Moisés mata un Egipcio [09-09] =====

 

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-09-09.jpg?nolink&}}

When **Moses** thought nobody would see, he killed the **Egyptian** and buried his body. But someone saw what Moses had done.

Cuando** Moisés ** pensó que nadie lo vería, él mató al **Egipcio ** y enterró su cuerpo. Pero alguien vio lo que** Moisés ** había hecho.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:moses|Moses]]**
  * **[[:es-419:obs:notes:key-terms:egypt|Egyptian]]**
==== Translation Notes: ====

* //(There are no notes for this frame.)//

**[[:es-419:obs:notes:frames:09-08|<<]] | [[:es-419:obs:notes:09|Up]] | [[:es-419:obs:notes:frames:09-10|>>]]**
