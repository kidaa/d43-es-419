===== Enemies Defeated Israel / Enemigos derrotaron Israel [16-02] B =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-16-02.jpg?nolink&}}

Because the **Israelites** kept **disobeying God**, he **punished** them by allowing their enemies to defeat them. These enemies stole things from the Israelites, destroyed their property, and killed many of them. After many years of disobeying God and being oppressed by their enemies, the Israelites **repented** and **asked God to rescue them**.

Debido a que los **israelitas** seguían **desobedecer a Dios**, él los** castigó** al permitir que sus enemigos les derrotaren. Estos enemigos robaron cosas de los hijos de **Israel, ** destruyeron su propiedad, y mataron a muchos de ellos. Después de muchos años de **desobedecer** a **Dios** y de ser oprimido por sus enemigos, los** israelitas** se **arrepintieron** y pidieron a **Dios ** que les rescataron.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
  * **[[:es-419:obs:notes:key-terms:disobey|disobeying]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:punish|punished]]**
  * **[[:es-419:obs:notes:key-terms:repent|repented]]**
==== Translation Notes: ====

  * **asked God to rescue them**  - That is, they asked God to help them and to set them free from their enemies.
  * **pidieron a Dios que los rescatara**-Eso es, que Ellos pidieron a Dios que los ayudara y los librara de sus enemigos.
**[[:es-419:obs:notes:frames:16-01|<<]] | [[:es-419:obs:notes:16|Up]] | [[:es-419:obs:notes:frames:16-03|>>]]**
