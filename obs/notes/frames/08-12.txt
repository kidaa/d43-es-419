===== Joseph Met His Brothers [08-12] / Jose conocio sus hermanos [08-12] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-12.jpg?nolink&}}

After **testing his brothers** to see **if they had changed**, **Joseph** said to them, “I am your brother, Joseph! **Do not be afraid**. You tried to do **evil** when you sold me as a **slave**, but **God** used the **evil for good!** Come and live in **Egypt** so I can provide for you and your families.”

Después de probar a sus hermanos para ver si habían cambiado, **José** les dijo, “¡Soy su hermano **José**! !No tengan miedo. Ustedes intentaron hacerme **mal** cuando me vendieron como** esclavo**, pero** Dios ** ha usado el **mal** por el **bien**! Vengan y vivan en **Egipto** para que yo pueda proveer para ustedes y sus familias.”

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:joseph|Joseph]]**
  * **[[:es-419:obs:notes:key-terms:evil|evil]]**
  * **[[:es-419:obs:notes:key-terms:servant|slave]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:good|good]]**
  * **[[:es-419:obs:notes:key-terms:egypt|Egypt]]**
==== Translation Notes: ====

  * **testing his brothers**  - Joseph placed his older brothers in a difficult situation to see if they would protect their youngest brother, or treat him as badly as they had treated Joseph. When they protected their youngest brother, Joseph knew they had changed.
  * **probando a sus hermanos** - Jose puso a sus hermanos en una situacion dificil para ver si ellos protejian a su hermano menor, o lo trataban tan mal como lo trataron a Jose. Cuando protejieron a su hermano menor, Jose se dio cuenta que habian cambiado. 
  * **if they had changed**  - Another way to say this would be, "if they were different than they used to be." Years ago Joseph's brothers sold him into slavery. Joseph wanted to find out if they would now do what was right.
  * **si ellos habian cambiado** - Otra manera de decir esto seria, " si es que ellos eran diferente a lo que eran antes." Anos pasados los hermanos de Jose lo vendieron a la esclavitud. Jose queria saber si ellos harian lo correcto esta ves. 
  * **do not be afraid**  - Another way to say this would be, "You do not need to fear any punishment from me." Joseph's brothers were afraid because they had greatly wronged Joseph and now as a great ruler he had the power to punish them. Joseph could refuse to sell them food, or even put them in prison or kill them.
  * **no tengan miedo**- Otra manera de decir esto es, "No necesitan tener miedo de algun castigo de parte mia." Los hermanos de Jose tenian miedo porque ellos habian inmensamente agraviado a Jose y ahora como gobernador tenia el poder de catigarlos. Jose podria negarles venderles comida, o ponerlos en la carcel or matarlos. 
  * **evil for good**  - Joseph's brothers did an evil thing when they sold Joseph as a slave and he was taken to Egypt. But God allowed this so that Joseph could save thousands of people from starving during the famine, including his own family. This was a very good thing.
  * **mal por bien**- Los hermanos de Jose hicieron halgo muy malo cuando vendieron a Jose como esclavo y fue llevado a Egipto. Pero Dios permitio esto para que Jose pudiera salvar de morir de hambre a miles de personas durante la hambrura, incluyendo su propia familia. Esto fue muy bueno. 
**[[:es-419:obs:notes:frames:08-11|<<]] | [[:es-419:obs:notes:08|Up]] | [[:es-419:obs:notes:frames:08-13|>>]]**
