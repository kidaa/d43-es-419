===== The Word of God [48-12]  La Palabra de Dios =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-48-12.jpg?nolink&}}

**Moses** was **a great prophet** who proclaimed the **word of God**. But **Jesus** is **the greatest prophet** of all. He is **God**, so everything he did and said were the actions and words of God. That is why Jesus is called the Word of God.

**Moisés ** era un profeta **grande ** que proclamó la palabra de Dios. Pero **Jesús ** es el profeta más grande de todos. El es **Dios**, asi, todo que él hizo y dijo fueron las acciones y palabras de **Dios**. Por esto Jesús es llamado la Palabra de Dios.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:moses|Moses]]**
  * **[[:es-419:obs:notes:key-terms:prophet|prophet]]**
  * **[[:es-419:obs:notes:key-terms:word-of-god|word of God]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
===== Translation Notes: =====


  * **a great prophet** - That is, “a very important prophet.”
  * **the greatest prophet** - That is, “the most important prophet.”
  * **He is the Word of God** - That is, “He is called the Word of God” because he reveals God's character. The other prophets preached the message God gave them, but Jesus revealed God in his preaching and his actions.

**[[:es-419:obs:notes:frames:48-11|<<]] | [[:es-419:obs:notes:48|Up]] | [[:es-419:obs:notes:frames:48-13|>>]]**
