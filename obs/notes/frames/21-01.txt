===== Messiah Will Defeat Satan [21-01]/Mesías Vencera a Satanas [21-01] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-21-01.jpg?nolink&}}

**From the very beginning**, **God** planned to send the **Messiah**. The first **promise** of the Messiah came to **Adam** and **Eve**. God promised that a **descendant** of Eve would be born who would **crush the snake’s head**. **The snake who deceived Eve was Satan**. The promise meant that the Messiah would defeat Satan completely.

Desde el principio, Dios planeó enviar al Mesías. La primera promesa del Mesías vino a Adán y Eva. Dios prometió que un descendiente de Eva nacería que aplastaría la cabeza de la serpiente. La serpiente que engañó a Eva era Satanás. La promesa significaba que el Mesías iba a derrotar a Satanás completamente.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:messiah|Messiah]]**
  * **[[:es-419:obs:notes:key-terms:promise|promise]]**
  * **[[:es-419:obs:notes:key-terms:adam|Adam]]**
  * **[[:es-419:obs:notes:key-terms:eve|Eve]]**
  * **[[:es-419:obs:notes:key-terms:descendant|descendant]]**
  * **[[:es-419:obs:notes:key-terms:satan|Satan]]**
==== Translation Notes: ====

  * **From the very beginning**  - That is, from when the earth was first created.
  * **Desde el principio **- Esto es, desde que la tierra fue creada.
  * **crush the snake's head**  - Unless the head of a venomous snake is crushed, the snake can still hurt someone. Use a word for "crush" that communicates that its head is destroyed.
  * **aplasto la cabeza de la serpiente** - A menos que la cabeza de una ****serpiente venenosa es aplastada, la serpiente todavia puede herir a alguien. Usar la palabra "aplastar" eso comunica que la cabeza fue destruida.
  * **The snake…was Satan**  – Satan spoke to Eve in the form of a snake. This does not mean that he is a snake now. This could be translated as, "The snake…was an appearance of Satan."
  * **La Serpiente…era Satanas **- Satanas le hablo a Eva en forma de serpiente. Esto no significa que el es una serpiente ahora. Esto puede ser traducido como, "La serpiente…estaba en una apariencia de Satanas."
  * **who deceived Eve**  - That is, "who lied to Eve." The snake lied by making Eve doubt what God had said and tricking her into disobeying God.
  * **quien engano a Eva **- Esto es,"quien le mintio a Eva." La serpiente mintio haciendola dudar que Dios dijo y la engano a que desobediera a Dios.
**[[:es-419:obs:notes:frames:20-13|<<]] | [[:es-419:obs:notes:21|Up]] | [[:es-419:obs:notes:frames:21-02|>>]]**
