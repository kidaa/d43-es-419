===== Sin and Deliverance / Pecado y Liberacion [16-17] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-16-17.jpg?nolink&}}

**This pattern repeated many times**: the **Israelites** would **sin**, **God** would **punish** them, they would **repent**, and God would send a **deliverer** to **save** them. Over many years, God sent many deliverers who saved the Israelites from their enemies.

Este patrón se repitió muchas veces: los **israelitas pecarían**, **Dios los castigaría**, ellos se** arrepentirían**, y **Dios** les enviaría un** libertador** para **salvarlos**. Durante muchos años, **Dios** envió a muchos **libertadores** que** salvaron** a los** israelitas** de sus enemigos.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
  * **[[:es-419:obs:notes:key-terms:sin|sin]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:punish|punish]]**
  * **[[:es-419:obs:notes:key-terms:repent|repent]]**
  * **[[:es-419:obs:notes:key-terms:deliverer|deliverer]]**
  * **[[:es-419:obs:notes:key-terms:save|save]]**
==== Translation Notes: ====

  * **This pattern repeated many times**  - This could be translated as, "These things kept happening again and again" or, "These things happened many times."
  * **Este modelo se repitio muchas veces **- esto puede ser traducido como, "Estas cosas se mantuvieron sucediendo una y otra vez" o, "Estas cosas sucedieron muchas veces".
**[[:es-419:obs:notes:frames:16-16|<<]] | [[:es-419:obs:notes:16|Up]] | [[:es-419:obs:notes:frames:16-18|>>]]**
