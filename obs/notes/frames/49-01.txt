===== The Virgin Birth [49-01] / El Nacimiento Virginal [49-01] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-01.jpg?nolink&}}

An angel told a **virgin** named **Mary** that she would give birth to **God's Son**. So while she was still a virgin, she gave birth to a son and named him **Jesus**. Therefore, Jesus is both God and human.

Un ángel dijo a una **virgen** se llamaba **María ** que ella iba a dar a a luz al **Hijo de Dios**. Así, minetras todavía era una **virgen**, ella dio a luz a un hijo y le nombró **Jesús**. Por lo tanto, **Jesús** es ambos Dios y hombre.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:virgin|virgin]]**
  * **[[:es-419:obs:notes:key-terms:mary|Mary]]**
  * **[[:es-419:obs:notes:key-terms:god-son|God's Son]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
===== Translation Notes: =====

  * //(There are no notes for this frame.)//
  * **(No hay notas para este marco.)**
**[[:es-419:obs:notes:frames:48-14|<<]] | [[:es-419:obs:notes:49|Up]] | [[:es-419:obs:notes:frames:49-02|>>]]**
