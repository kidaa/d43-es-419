===== Worship for the Baby [23-10] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-23-10.jpg?nolink&}}

When the** wise men** saw Jesus with his mother, they **bowed down** and **worshiped** him. They gave Jesus **expensive** gifts. Then they returned home.

Cuando los sabios vieron a Jesús con su madre, se postraron, y le adoraron. Le dieron a Jesús regalos costosos. Luego volvieron a casa.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:worship|worshiped]]**
===== Translation Notes: =====

  * **wise men**  - See how you translated this term in **[[:es-419:obs:notes:frames:23-09|[23-09]]]**.
  * **Los hombres sabios** - Mira como tu traducistes este termino en [23-09]
  * **bowed down**  - That is, "bowed low to the ground." At that time, this was the customary way of showing great respect or reverence.
  * **postraron **- Esto es, "se inclino hasta al suelo." A este tiempo, esta era la costumbre de ensenar grande respeto o reverencia.
  * **expensive**  - That is, "very valuable."
  * **costoso **- Esto es, "mucho valor."
  //A Bible story from//  - These references may be slightly different in some Bible translations.
  
 //Una historia de la Biblia// - Estas referencias pueden ser un poco diferentes en algunas traducciones de la Biblia.

 //Una historia de la Biblia de: Mateo 1; Lucas 2//

**[[:es-419:obs:notes:frames:23-09|<<]] | [[:es-419:obs:notes:23|Up]] | [[:es-419:obs:notes:frames:24-01|>>]]**
