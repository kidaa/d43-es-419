===== God Sent Moses to Egypt Dios envía a Moisés a Egipcio [09-13]  =====

 

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-09-13.jpg?nolink&}}

**God** said, “I have seen the **suffering of my people**. I will send you to **Pharaoh** so that you can **bring the Israelites out of their slavery in Egypt**. I will give them the land of **Canaan**, the land I **promised** to **Abraham**, **Isaac**, and **Jacob**.”

Dios le dijo, “He visto el **sufrimiento** de mi gente. Te enviaré al **Faraón** para que pueda sacar a los **Israelitas** de su **esclavitud** en **Egipto**. Les daré a ellos la tierra de** Canaan**, la tierra que yo **prometí**a **Abraham**, **Isaac**, y **Jacob**.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:suffer|suffering]]**
  * **[[:es-419:obs:notes:key-terms:pharaoh|Pharaoh]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
  * **[[:es-419:obs:notes:key-terms:servant|slavery]]**
  * **[[:es-419:obs:notes:key-terms:egypt|Egypt]]**
  * **[[:es-419:obs:notes:key-terms:canaan|Canaan]]**
  * **[[:es-419:obs:notes:key-terms:promise|promised]]**
  * **[[:es-419:obs:notes:key-terms:abraham|Abraham]]**
  * **[[:es-419:obs:notes:key-terms:isaac|Isaac]]**
  * **[[:es-419:obs:notes:key-terms:jacob|Jacob]]**
==== Translation Notes: ====

  * **suffering of my people**  - This could be translated as, "the very harsh treatment that my people are experiencing. Some languages might translate this as, "how the Egyptians are giving my people terrible pain."
  * **sufrimiento de mi pueblo**  - Esto puede ser traducido como, ¨el tratamiento muy duro que mi pueblo esta experimentando.Algunas lenguajes puede traducir esto como, ¨ cómo los egipcios están dado a mi pueblo un terrible dolor¨.
  * **my people**  - This refers to the Israelites. God had made a covenant with Abraham and his descendants that He would bless them and make them into a great nation. Through this covenant, the Israelites became God's own people.
  * **mi pueblo**  - Esto se refiere a los Israelitas. Dios había hecho un pacto con Abraham y que El bendeciria a sus descendientes y haría de ellos una gran nación. A través de este pacto, los Israelitas se volverían su propio pueblo.
  * **bring … out of their slavery in Egypt**  - This can be translated as, "set them free from being slaves in Egypt" or, "bring them out of Egypt where they are now slaves."
  * **traer… fuera de su esclavitud en Egipto**  - Esto puede ser traducido como, ¨ hacerlo a ellos libre de la esclavitud en Egipto¨ o, ¨traer a ellos fuera de Egipto donde ellos son esclavos¨.
**[[:es-419:obs:notes:frames:09-12|<<]] | [[:es-419:obs:notes:09|Up]] | [[:es-419:obs:notes:frames:09-14|>>]]**
