===== Judah Was Taken to Babylon [20-09]Judá fue Tomada por Babilonia [20-09] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-20-09.jpg?nolink&}}

**Nebuchadnezzar** and his army took almost all of the people of the **kingdom of Judah** to **Babylon**, **leaving only the poorest people behind** to plant the fields. **This period of time** when **God's** people were forced to leave the **Promised Land** is called the **Exile**.

Nebucodonazor y su ejército tomó casi todos los pueblos del reino de Judá a Babilonia, dejando sólo los más pobres detrás de sembrar los campos. Este período de tiempo en que el pueblo de Dios se vieron obligados a abandonar la tierra prometida se llama el Exilio.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:nebuchadnezzar|Nebuchadnezzar]]**
  * **[[:es-419:obs:notes:key-terms:kingdom-of-judah|kingdom of Judah]]**
  * **[[:es-419:obs:notes:key-terms:babylon|Babylon]]**
  * **[[:es-419:obs:notes:key-terms:god|God's]]**
  * **[[:es-419:obs:notes:key-terms:promised-land|Promised Land]]**
==== Translation Notes: ====

  * **leaving…behind**  - That is, "leaving only the poorest people in Judah" or, "letting only the poorest people stay in Judah."
  * **dejando…atras** - Esto es, "dejando solamente a los mas pobres en Juda" o, "dejando solamente a los mas pobres en Juda."
  * **This period of time**  - To translate this, choose a phrase that can refer to a long time, since this exile period lasted seventy years.
  * **Este periodo de tiempo** - Para traducir esto, escoge una frase que pueda referirse a un tiempo largo, desde el periodo de exilio que duro setenta anos.
  * **Exile**  - The word "exile" means someone is removed from they country by force. The "Exile" is the term for this 70-year period when the Israelites were forced to live in Babylon.
  * **Exilio **- La palabra "exilio" significa alguien que es sacado de su pais a la fuerza. Este "Exilio" es un termino de un periodo de setenta anos cuando los Israelitas fueron forzados a vivir en Babilonia.
**[[:es-419:obs:notes:frames:20-08|<<]] | [[:es-419:obs:notes:20|Up]] | [[:es-419:obs:notes:frames:20-10|>>]]**
