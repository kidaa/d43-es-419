===== David Murdered Uriah [17-12] / David Mato Urias [17-12] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-17-12.jpg?nolink&}}

**Bathsheba's** husband, a man named **Uriah**, was one of **David's** best soldiers. David called Uriah back from the battle and told him to **go be with his wife**. But Uriah refused to go home while the rest of the soldiers were in battle. So David sent Uriah back to the battle and told the general to place him **where the enemy was strongest** so that he would be killed.

El marido de **Betsabé**, un hombre llamado **Urías**, fue uno de los mejores soldados de **David.** **David** llamó a** Urías** de vuelta de la batalla y le dijo que fuera para estar con su esposa. Pero** Urías** se negó a ir a casa, mientras que el resto de los soldados estaban en la batalla. Así **,David** envió a **Urías** de vuelta a la batalla y dijo al general le coloque donde el enemigo era más fuerte para que lo matarían.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:bathsheba|Bathsheba's]]**
  * **[[:es-419:obs:notes:key-terms:uriah|Uriah]]**
  * **[[:es-419:obs:notes:key-terms:david|David's]]**
==== Translation Notes: ====

  * **go be with his wife**  - That is, "go home to be intimate with his wife." David wanted people, especially Uriah, to believe that Bathsheba was pregnant with Uriah's child.
  * **fuera para estar con su mujer** - Eso es, "vete a la casa para estar intimamente con su mujer." David queria que la gente, especialmente Urías, que creyeran que Betsabé estaba embarazada con el hijo de Urías. 
  * **where the enemy was strongest**  - That is, the location in the battle where the most fighting was going on.
  * **donde el enemigo estaba mas fuerte** - Eso es, la localización en la batalla donde mas peleando estaba sucediendo.
**[[:es-419:obs:notes:frames:17-11|<<]] | [[:es-419:obs:notes:17|Up]] | [[:es-419:obs:notes:frames:17-13|>>]]**
