===== Rehoboam Became King [18-05] Roboam se convirtio en Rey [18-05] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-18-05.jpg?nolink&}}

After **Solomon** died, his son, **Rehoboam**, became **king**. Rehoboam was a foolish man. All the people of the **nation of Israel** came together to **confirm him as king**. They complained to Rehoboam that Solomon had made them do a lot of hard work and pay a lot of taxes.

Después de la muerte de **Salomón**, su hijo **Roboam**, se convirtió en **rey**. **Roboam** era un hombre insensato. Todo el pueblo de la nación de **Israel** se reunieron para confirmarlo como** rey**. Se quejaron a **Roboam** que **Salomón** había hecho hacer un montón de trabajo duro y pagaron muchos impuestos.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:solomon|Solomon]]**
  * **[[:es-419:obs:notes:key-terms:rehoboam|Rehoboam]]**
  * **[[:es-419:obs:notes:key-terms:king|king]]**
  * **[[:es-419:obs:notes:key-terms:israel|nation of Israel]]**
==== Translation Notes: ====

  * **confirm him as king**  - That is, "tell him that they were glad he was king and that they would do what he said."
  * **confirmo a el como el rey - **  Esto se, "dile a elque ellos estaban contentos que era el rey y iban a hacer lo que el dijera."
**[[:es-419:obs:notes:frames:18-04|<<]] | [[:es-419:obs:notes:18|Up]] | [[:es-419:obs:notes:frames:18-06|>>]]**
