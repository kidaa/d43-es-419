===== The Death of the Messiah [21-12] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-21-12.jpg?nolink&}}

The **prophets** also told about how the **Messiah** would **die**. **Isaiah prophesied** that people would spit on, mock, and beat the Messiah. They would **pierce** him and he would die in great suffering and agony, even though he had not done anything wrong.

Los profetas también, hablaron de cómo el Mesías iba a morir. Isaías profetizó que la gente escupiría en él, maquetarlo, y batir el Mesías. Ellos le atravesaría y moriría en un gran sufrimiento y la agonía, a pesar de que él no había hecho nada malo.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:prophet|prophets]]**
  * **[[:es-419:obs:notes:key-terms:messiah|Messiah]]**
  * **[[:es-419:obs:notes:key-terms:death|die]]**
  * **[[:es-419:obs:notes:key-terms:isaiah|Isaiah]]**
  * **[[:es-419:obs:notes:key-terms:prophet|prophesied]]**
==== Translation Notes: ====

  * **pierce**  - That is, "stab." Sharp objects would penetrate his body as part of the punishment for people's sin.
  * **atravesar **- Esto es, "apunalar" Objetos afilado penetraran su cuerpo como parte del castigo por el pecado de la gente.
**[[:es-419:obs:notes:frames:21-11|<<]] | [[:es-419:obs:notes:21|Up]] | [[:es-419:obs:notes:frames:21-13|>>]]**
