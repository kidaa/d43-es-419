===== The Two Spies / Dos Espías [15-01] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-15-01.jpg?nolink&}}

**At last it was time for** the **Israelites** to **enter Canaan**, the **Promised Land**. **Joshua** sent **two spies to the Canaanite city of Jericho** that was **protected by strong walls**. In that city there lived a prostitute named **Rahab** who hid the spies and later helped them to **escape**. She did this because she **believed God**. They promised to protect Rahab and **her family** when the Israelites would destroy Jericho.

Por fin llegó el momento para los** Israelitas** para entrar en **Canaán**, la **Tierra Prometida**. **Josué**envió a dos espías a la ciudad **Cananea** de **Jericó** que estaba protegida por fuertes muros. En esa ciudad vivía una prostituta llamada **Rahab** que escondió a los espías y luego los ayudó a escapar. Ella hizo esto porque ella creyó a** Dios**. Ellos **prometieron** proteger a **Rahab** y a su familia, cuando los hijos de **Israel ** destruiría **Jericó**.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
  * **[[:es-419:obs:notes:key-terms:canaan|Canaan]]**
  * **[[:es-419:obs:notes:key-terms:promised-land|Promised Land]]**
  * **[[:es-419:obs:notes:key-terms:joshua|Joshua]]**
  * **[[:es-419:obs:notes:key-terms:canaan|Canaanite]]**
  * **[[:es-419:obs:notes:key-terms:jericho|Jericho]]**
  * **[[:es-419:obs:notes:key-terms:rahab|Rahab]]**
  * **[[:es-419:obs:notes:key-terms:believe|believed]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:promise|promised]]**
==== Translation Notes: ====

  * **At last it was time for**  - "At last" means "finally" or, "after a long wait." To make it clear what "time" refers to, you could say, "after they had wandered in the desert for 40 years, God finally permitted."
  * **Por fin era tiempo para** - "Por fin" significa "finalmente" o, "despues de una espera larga. "Para hacerlo claro que "tiempo" se refiere a, "despues que ellos habian vagado en el desierto por 40 años, Dios finalmente permitio." 
  * **two spies to the Canaanite city of Jericho**  - This could be translated as, "two men to Jericho, a city in Canaan, to find out information about it." Also see the notes for, "spy out the land" in **[[:es-419:obs:notes:frames:14-04|[14-04]]]**
  * **dos espias al la ciudad Canaanita de Jericó **- Esto puede ser traducido como, "dos espias a Jerico, una ciudad de Canaan, para encontrar informacion sobre ella." Tambien ve las notas para, "vigila la tierra" en
  * **protected by strong walls**  - This could be translated as, "completely surrounded by thick, strong walls made of stone to protect it from their enemies."
**protegidos por paredes fuertes** - Esto puede ser traducido como, "completamente rodeado por gruesas y fuertes paredes hechas de piedra para protegerla de sus enemigos."
  * **escape**  – It is also possible to add, "escape from people in Jericho who wanted to harm them."
  * **escapar** - Tambien es possible agregar, "escapar de la gente de Jericó que querian lastimarlos."
  * **her family**  - Rahab asked for protection for her father, mother, brothers, and sisters. Use your word for family that includes these people.
  * **su familia** - Rahab pidio por protecion para su padre, madre, hermanos, y hermanas. Usa tu palabra para familia que incluye esta gente.
**[[:es-419:obs:notes:frames:14-15|<<]] | [[:es-419:obs:notes:15|Up]] | [[:es-419:obs:notes:frames:15-02|>>]]**
