===== Unable to Speak [22-03] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-22-03.jpg?nolink&}}

The **angel** responded to Zechariah, “I was sent by **God** to bring you this good news. Because you did not **believe** me, you will not be able to speak until the child is born.” Immediately, **Zechariah** was unable to speak. Then the angel left Zechariah. After this, Zechariah returned home and his wife **became pregnant**.

El ángel respondió a Zacarías, “He sido enviado por Dios para darte estas buenas noticias. Porque usted no me cree, usted no será capaz de hablar hasta que nazca el niño.” Inmediatamente, Zacarías no pudo hablar. Y el ángel se fue de Zacarías. Después de esto, Zacarías regresó a su casa y su esposa quedó embarazada.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:angel|angel]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:believe|believe]]**
  * **[[:es-419:obs:notes:key-terms:zechariah-nt|Zechariah]]**
===== Translation Notes: =====

  * **became pregnant**  - Some languages have polite ways of saying "become pregnant." Use an expression that will not be embarrassing to readers.
  * **salio embarazada - En algunos idiomas tienen formas educadas de decir "salio embarazada." Use una expression que no sea embarazoso a los lectores.**
**[[:es-419:obs:notes:frames:22-02|<<]] | [[:es-419:obs:notes:22|Up]] | [[:es-419:obs:notes:frames:22-04|>>]]**
