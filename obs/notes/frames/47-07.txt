===== Paul and Silas Were Arrested [47-07] Pablo y Silas Feuron Arrestados =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-47-07.jpg?nolink&}}

So the owners of the **slave girl** took **Paul** and **Silas** to the **Roman authorities**, who beat them and **threw them** into jail.

Así, los dueños de la niña esclava llevaron a Pablo y a Sílas a las autoridades Romanas, quienes les azotaron y les echaron a la carcel.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:servant|slave girl]]**
  * **[[:es-419:obs:notes:key-terms:paul|Paul]]**
  * **[[:es-419:obs:notes:key-terms:silas|Silas]]**
  * **[[:es-419:obs:notes:key-terms:rome|Roman authorities]]**
===== Translation Notes: =====

  * **threw them**  - That is, "put them."
  * **los echaron **- Esto es, "ponerlos."
**[[:es-419:obs:notes:frames:47-06|<<]] | [[:es-419:obs:notes:47|Up]] | [[:es-419:obs:notes:frames:47-08|>>]]**
