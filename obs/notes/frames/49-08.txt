===== God Hates Sin [49-08] /Dios Odia el Pecado [49-08] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-08.jpg?nolink&}}

**Jesus** also told us that **God** hates **sin**. When **Adam** and **Eve sinned**, it affected all of their **descendants**. As a result, every person in the world sins and **is separated from God**. Therefore, everyone has become an enemy of God.

Jesús también, nos dijo que Dios odia el pecado. Cuando Adán y Eva pecaron, esto afectó a todos sus decendientes. Como resultado, toda persona en el mundo peca y está separada de Dios. Por lo tanto, toda persona ha llegado a ser un enemigo de Dios.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:sin|sin]]**
  * **[[:es-419:obs:notes:key-terms:adam|Adam]]**
  * **[[:es-419:obs:notes:key-terms:eve|Eve]]**
  * **[[:es-419:obs:notes:key-terms:sin|sinned]]**
  * **[[:es-419:obs:notes:key-terms:descendant|descendants]]**
===== Translation Notes: =====

  * **is separated from God**  - This can also be translated as, "cannot live with God" or, "cannot be near God" or, "cannot have a relationship with God."
  * **es separado de Dios **- Esto tambien puede ser traducido como, "no puede vivir con Dios" o, " no puede tener una relacion con Dios"
**[[:es-419:obs:notes:frames:49-07|<<]] | [[:es-419:obs:notes:49|Up]] | [[:es-419:obs:notes:frames:49-09|>>]]**
