===== Our Crown [50-13]Nuestra Corona [50-13] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-50-13.jpg?nolink&}}

**Jesus promised** to give a **crown** to everyone who **believes** in him. They will **live** and reign with **God** in **perfect peace** forever.

Jesús prometió que dará una corona a cada persona que cree en él. Ellos vivirán y reinarán con Dios en perfecta paz para siempre.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:promise|promised]]**
  * **[[:es-419:obs:notes:key-terms:believe|believes]]**
  * **[[:es-419:obs:notes:key-terms:life|live]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:peace|peace]]**
===== Translation Notes: =====

  * **crown**  - This crown represents our reward for believing in Jesus and serving him in this life.
  * **corona -**  Esta corona representa nuestra recompensa por creyiendo en Jesús y sirviendole en esta vida.
  * **perfect**  - That is, "complete" or, "total."
  * **perfecto - ** Eso es, "completo" o, "total."
**[[:es-419:obs:notes:frames:50-12|<<]] | [[:es-419:obs:notes:50|Up]] | [[:es-419:obs:notes:frames:50-14|>>]]**
