===== Angels Ministered to Jesus [25-08] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-25-08.jpg?nolink&}}

**Jesus did not give in to Satan's temptations**, so Satan left him. Then **angels** came and took care of Jesus.

Jesús no ceder a las tentaciones de Satanás, por lo que Satanás le dejó. Entonces, los ángeles vinieron y cuidaron de Jesús.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:satan|Satan's]]**
  * **[[:es-419:obs:notes:key-terms:tempt|temptations]]**
  * **[[:es-419:obs:notes:key-terms:angel|angels]]**
===== Translation Notes: =====

  * **did not give in to**  - This could be translated as, "did not do the things that Satan was tempting him to do."
  * no se cedio a - Esto puede ser traducido como, "no hizo las cosas que satanas lo tentaba  a hacer
  * //A Bible story from//  - These references may be slightly different in some Bible translations.
  * //Una historia de la Biblia de: Mateo 4: 1-11; Marcos 1: 12-13; Lucas 4: 1-13//
**[[:es-419:obs:notes:frames:25-07|<<]] | [[:es-419:obs:notes:25|Up]] | [[:es-419:obs:notes:frames:26-01|>>]]**
