===== Joseph Was Alive [08-13] / Jose esta vivo [08-13] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-13.jpg?nolink&}}

When **Joseph's** brothers returned home and told their father, **Jacob**, that Joseph was still alive, he was very happy.

Cuando los hermanos de **José** volvieron a casa y le dijeron a su padre, **Jacob**, que** José** estaba todavía vivo, él estuvo muy contento.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:joseph|Joseph's]]**
  * **[[:es-419:obs:notes:key-terms:jacob|Jacob]]**
==== Translation Notes: ====

//(There are no notes for this frame.)//

**[[:es-419:obs:notes:frames:08-12|<<]] | [[:es-419:obs:notes:08|Up]] | [[:es-419:obs:notes:frames:08-14|>>]]**
