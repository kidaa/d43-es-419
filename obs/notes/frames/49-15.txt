===== God's Kingdom of Light [49-15]Reino de Dios de Luz [49-15] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-49-15.jpg?nolink&}}

If you **believe** in **Jesus** and what he has done for you, you are a **Christian**! **God** has taken you out of **Satan's kingdom of darkness** and put you into **God’s kingdom of light**. God has taken away your old, sinful ways of doing things and has given you new, righteous ways of doing things.

¡Si cree en Jesús y lo que ha hecho por Ud., Ud. es un Cristiano! Dios le ha quitado del reino de oscuridad de Satanás y le ha colocado en el reino de luz de Dios. Dios ha quitado su manera vieja pecaminosa de hacer las cosas y le ha dado una manera nueva y justa de hacer las cosas.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:believe|believe]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:christian|Christian]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:satan|Satan's]]**
  * **[[:es-419:obs:notes:key-terms:kingdom|kingdom]]**
  * **[[:es-419:obs:notes:key-terms:kingdom-of-god|God's kingdom]]**
===== Translation Notes: =====

  * **Satan's kingdom of darkness**  - "Darkness" is used here to refer to sin and everything that is evil. This could be translated as, "Satan's evil rule over people, which is like darkness."
  * **Oscuridad el reino de Satanas **- " Oscuridad" se usa aqui con referencia a el pecado y todo lo que es malo. Esto puede ser traducido como, "Satanas domina sobre su gente, que es como oscuridad."
  * **God's kingdom of light**  - "Light" here refers to God's holiness and goodness. This could be translated as, "God's righteous rule over people, which is like light." The Bible often compares evil to darkness, and goodness to light.
  * **Reino de Dios is Luz **- "Luz aqui se refiere a la santidad y bondad de Dios. Esto puede ser traducido como, "Dios manda con justificacion y virtud. que es como la luz" La biblia compara malo con oscuridad y bueno con la luz.
**[[:es-419:obs:notes:frames:49-14|<<]] | [[:es-419:obs:notes:49|Up]] | [[:es-419:obs:notes:frames:49-16|>>]]**
