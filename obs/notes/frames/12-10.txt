===== God Fought for Israel [12-10] / **Dios Peleo por Israel [12-10]**  =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-12-10.jpg?nolink&}}

So they followed the **Israelites** onto the **path through the sea**, but **God** caused the **Egyptians** to panic and caused their **chariots** to **get stuck**. They shouted, “Run away! God is fighting for the Israelites!”

Así que, ellos siguieron a los **Israelitas ** por el camino en el mar, pero **Dios** causó que los** Egipcios** tuvieran pánico y causó que sus carros se atascaran. Ellos gritaron, “​!Corran! ¡**Dios** está peleando por los **Israelitas**!
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:egypt|Egyptians]]**
  * **[[:es-419:obs:notes:key-terms:chariot|chariots]]**
==== Translation Notes: ====

  * **path through the sea**  - This was the dry strip of land across the bottom of the sea, with a wall of water on each side.
  * **camino a traves del mar** - Esto era una zona de tierra seca a traves del fondo del mar, con una pared de agua en cado lado. 
  * **to panic**  – This could be translated as, "to get fearful and confused."
  * **panico **- Esto puede ser traducido como, ponerse temeroso y confundido."
  * **get stuck**  - The chariots could no longer move.
  * **atascarse** - Los ****ca****rros**** no se podian mover.
 
**[[:es-419:obs:notes:frames:12-09|<<]] | [[:es-419:obs:notes:12|Up]] | [[:es-419:obs:notes:frames:12-11|>>]]**
