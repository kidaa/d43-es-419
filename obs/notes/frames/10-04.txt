===== The Frogs [10-04] / Las Ranas [10-04] =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-10-04.jpg?nolink&}}

**God sent frogs all over Egypt**. **Pharaoh begged Moses** to take away the frogs. But after all the frogs died, Pharaoh **hardened his heart** and would not let the **Israelites** leave Egypt.

**Dios** envió** ** ranas  en todo **Egipto**. El **Faraón** rogó a** Moisés ** que quitara las ranas. Pero después de morir todas las ranas, el **Faraón** endureció su corazón y no dejó que los **Israelitas** sali eran de **Egipto**.
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:god|God]]**
  * **[[:es-419:obs:notes:key-terms:egypt|Egypt]]**
  * **[[:es-419:obs:notes:key-terms:pharaoh|Pharaoh]]**
  * **[[:es-419:obs:notes:key-terms:beg|begged]]**
  * **[[:es-419:obs:notes:key-terms:moses|Moses]]**
  * **[[:es-419:obs:notes:key-terms:israel|Israelites]]**
==== Translation Notes: ====

  * **God sent frogs all over Egypt**  - This could be translated as, "God caused many frogs to appear throughout Egypt."
  * **Dios envío ranas por todo Egipto: **  Esto podría traducirse como: "Dios hizo que muchas ranas aparecieran por todo Egipto".
  * **hardened his heart**  - He became stubborn again and refused to obey God.
  * **endureció su corazón: **  El se volvió necio otra vez y se negó a obedecer a Dios.
**[[:es-419:obs:notes:frames:10-03|<<]] | [[:es-419:obs:notes:10|Up]] | [[:es-419:obs:notes:frames:10-05|>>]]**
