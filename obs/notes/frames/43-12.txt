===== 3000 Believe [43-12] / 3000 Creen =====

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-43-12.jpg?nolink&}}

About 3,000 people **believed** what **Peter** said and became **disciples** of **Jesus**. They were **baptized** and became part of the **church** at **Jerusalem**.

Como 3.000 personas creyeron lo que Pedro dijo y llegaron a ser discípulos de Jesús. Fueron bautizadas y llegaron a ser parte de la iglesia a Jerusalen.

===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:believe|believed]]**
  * **[[:es-419:obs:notes:key-terms:peter|Peter]]**
  * **[[:es-419:obs:notes:key-terms:disciple|disciples]]**
  * **[[:es-419:obs:notes:key-terms:jesus|Jesus]]**
  * **[[:es-419:obs:notes:key-terms:baptize|baptized]]**
  * **[[:es-419:obs:notes:key-terms:church|church]]**
  * **[[:es-419:obs:notes:key-terms:jerusalem|Jerusalem]]**
===== Translation Notes: =====

  * //(There are no notes for this frame.)//
  * //**(No hay notas para este marco.)** //
**[[:es-419:obs:notes:frames:43-11|<<]] | [[:es-419:obs:notes:43|Up]] | [[:es-419:obs:notes:frames:43-13|>>]]**
