===== Joseph Ruled Egypt [08-08] José gobernó Egipto [08-08]=====


{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-08-08.jpg?nolink&}}

**Pharaoh** was so **impressed with Joseph** that he appointed him to be the **second most powerful man** in all of **Egypt**!

¡El **Faraón ** estaba tan impresionado con **José** que le nombró como el segundo hombre más poderoso de **Egipto**!
===== Important Terms: =====

  * **[[:es-419:obs:notes:key-terms:pharaoh|Pharaoh]]**
  * **[[:es-419:obs:notes:key-terms:joseph|Joseph]]**
  * **[[:es-419:obs:notes:key-terms:egypt|Egypt]]**
==== Translation Notes: ====

  * **impressed with**  - Pharaoh was amazed by Joseph's wisdom and felt respect for him; he trusted Joseph to make wise decisions that would benefit the people. It may be clearer to say, "impressed with Joseph's wisdom."
  * **impresionado con - **  Faraón estaba asombrado con la sabiduría de José y le tubo respeto; el confiaba que José podría tomar decisiones sabias que beneficiarían a la gente. Seria mas claro decir, "impresionado con la sabiduría de José**."**
  * **second most powerful man**  - Pharaoh made Joseph a very powerful and important ruler over all of Egypt. Only Pharaoh was more powerful and important than Joseph.
  * **el segundo hombre mas poderoso - **  Faraón le dio mucho poder a José y un gobernador muy importante sobre todo Egipto. Solamente Faraón era mas poderoso e importante que José.
**[[:es-419:obs:notes:frames:08-07|<<]] | [[:es-419:obs:notes:08|Up]] | [[:es-419:obs:notes:frames:08-09|>>]]**
