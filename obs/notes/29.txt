====== 29. The Story of the Unmerciful Servant / La Historia de un Sirviente Cruel ======

===== Overview: =====

Peter asked Jesus how many times he should forgive a brother. So Jesus told the story of a servant who owed a huge debt to the king. The king took pity on him and cancelled the debt. But the servant sent another man who owed him a small amount to prison. This made the king very angry. Jesus told Peter that we should forgive others in the same way that we are forgiven.

====== Translate the Story ======

Click on the links below to translate each frame of the story.

[[:es-419:obs:notes:frames:29-01|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-01.jpg?nolink&200}}]]\\
How Many Times? [29-01]

[[:es-419:obs:notes:frames:29-02|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-02.jpg?nolink&200}}]]\\
The Huge Debt [29-02]

[[:es-419:obs:notes:frames:29-03|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-03.jpg?nolink&200}}]]\\
Not Able to Repay [29-03]

[[:es-419:obs:notes:frames:29-04|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-04.jpg?nolink&200}}]]\\
The Debt Is Canceled [29-04]

[[:es-419:obs:notes:frames:29-05|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-05.jpg?nolink&200}}]]\\
The Small Debt [29-05]

[[:es-419:obs:notes:frames:29-06|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-06.jpg?nolink&200}}]]\\
To Prison [29-06]

[[:es-419:obs:notes:frames:29-07|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-07.jpg?nolink&200}}]]\\
The King Heard [29-07]

[[:es-419:obs:notes:frames:29-08|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-08.jpg?nolink&200}}]]\\
The Wicked Servant [29-08]

[[:es-419:obs:notes:frames:29-09|{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-29-09.jpg?nolink&200}}]]\\
Forgive Your Brother [29-09]

//A Bible story from: Matthew 18:21-35//

**[[:es-419:obs:notes:28|<< Previous]] | [[:es-419:obs:notes:30|Next >>]]**
