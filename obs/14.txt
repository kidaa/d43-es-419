====== 14. Wandering in the Wilderness ======

====== 14. Vagando en el desierto ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-01.jpg?direct&}}

After God had told the Israelites the laws he wanted them to obey as part of his covenant with them, they left Mount Sinai. God began leading them from toward the Promised Land, which was also called Canaan. The pillar of cloud went ahead of them towards Canaan and they followed it.

Después que Dios le dijo a los israelitas las leyes que Él quería que obedecieran como parte de su pacto, ellos dejaron el Monte Sinaí. Dios empezó a guiarles hacia la tierra prometida, que fue llamada también, Canaán. La columna de nube iba delante de ellos hacia Canaán y ellos la seguían.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-02.jpg?direct&}}

God had promised Abraham, Isaac, and Jacob that he would give the Promised Land to their descendants, but now there were many people groups living there. They were called Canaanites. The Canaanites did not worship or obey God. They worshiped false gods and did many evil things.

Dios había prometido a Abraham, a Isaac, y a Jacob que Él le daría la tierra prometida a sus descendientes, pero ahora había muchas personas viviendo allí. Ellos eran llamados los cananeos. Los cananeos no obedecían ni adoraban a Dios. Ellos adoraban a dioses falsos y hacían muchas cosas malas.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-03.jpg?direct&}}

God told the Israelites, "You must get rid of all the Canaanites in the Promised Land. Do not make peace with them and do not marry them. You must completely destroy all of their idols. If you do not obey me, you will worship their idols instead of me."

Dios dijo a los israelitas, "Ustedes deben acabar con todos los cananeos en la tierra prometida. No hagan la paz con ellos ni se casen con ellos. Deben destruir completamente todos sus ídolos. Si no me obedecen, adorarán sus ídolos en lugar de mí."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-04.jpg?direct&}}

When the Israelites reached the border of Canaan, Moses chose twelve men, one from each tribe of Israel. He gave the men instructions to go and spy on the land to see what it was like. They were also to spy on the Canaanites to see if they were strong or weak.

Cuando los israelitas llegaron a la frontera de Canaán, Moisés escogió a doce hombres, uno de cada tribu de Israel. El dió a los hombres instrucciones para ir y espiar la tierra para ver como era. También, tenían que espiar a los cananeos para ver si eran fuertes o débiles.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-05.jpg?direct&}}

The twelve men traveled through Canaan for forty days and then they came back. They told the people, "The land is very fertile and the crops are plentiful!" But ten of the spies said, "The cities are very strong and the people are giants! If we attack them, they will certainly defeat us and kill us!"

Los doce hombres viajaron a través de Canaán por cuarenta días y entonces, volvieron. Dijeron a la gente, "¡La tierra es muy fértil y los cultivos son abundantes!" Pero diez de los espías dijeron, "¡Las ciudades son muy fuertes y las personas son gigantes! "¡Si les atacamos, ciertamente nos vencerán y nos matarán!"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-06.jpg?direct&}}

Immediately Caleb and Joshua, the other two spies, said, "¡It is true that the people of Canaan are tall and strong, but we can certainly defeat them! God will fight for us!"

Inmediatamente, Caleb y Josué, los otros dos espías, dijeron, "¡Es verdad que la gente de Canaán son altos y fuertes, pero ciertamente podemos vencerles! ¡Dios peleará por nosotros!"

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-07.jpg?direct&}}

But the people did not listen to Caleb and Joshua. They became angry with Moses and Aaron and said, "Why did you bring us to this horrible place? We should have stayed in Egypt rather than be killed in battle and our wives and children made slaves." The people wanted to choose a different leader to take them back to Egypt.

Pero la gente no escucharon a Caleb ni a Josué. Se enojaron con Moisés y a Aarón y les dijeron, "¿Por qué nos trajeron a este lugar horrible? Habría sido mejor quedarnos en Egipto el lugar de ser asesinados en batalla y  nuestra mujeres e hijos ser hechos esclavos." La gente quería escoger a un líder diferente para llevarles de vuelta a Egipto.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-08.jpg?direct&}}

God was very angry and came to the Tent of Meeting. God said, "Because you have rebelled against me, all of the people will have to wander in the wilderness. Except for Joshua and Caleb, everyone who is twenty years or older will die there and never enter the Promised Land."

Dios estaba muy enojado y vino al tienda de reunión. Dios dijo, "Porque ustedes se han rebelado contra mí, toda la gente vagará en el desierto. A excepción de Josué y Caleb, todos que tienen veinte años o más morirán allá y nunca entrarán en la Tierra Prometida."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-09.jpg?direct&}}

When the people heard this, they were sorry they had sinned. They took their weapons and went to attack the people of Canaan. Moses warned them not to go because God was not with them, but they did not listen to him.

Cuando la gente oyó esto, estaban tristes por que habían pecado. Tomaron sus armas y fueron para atacar a la gente de Canaán.  Moisés les advirtió que no fueran porque Dios no estaba con ellos, pero ellos no le escucharon.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-10.jpg?direct&}}

God did not go with them into this battle, so they were defeated and many of them were killed. Then the Israelites turned back from Canaan and wandered through the wilderness for forty years.

Dios no fue con ellos a esta batalla, así, ellos que fueron vencidos y muchos de ellos fueron asesinados. Entonces, los israelitas regresaron de Canaán y vagaron por el desierto por cuarenta años.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-11.jpg?direct&}}

During the forty years that the people of Israel wandered in the wilderness, God provided for them. He gave them bread from heaven, called "manna." He also sent flocks of quail (which are medium-sized birds) into their camp so they could have meat to eat. During all that time, God kept their clothes and sandals from wearing out.

Durante los cuarenta años que la gente de Israel vagaba en el desierto, Dios proveía para ellos. Les dió pan del cielo, que se llamaba "maná." También, envió bandadas de codornices (que son pajaros de tamaño medio) a su campamento para que ellos podrían tener carne para comer. Durante todo aquel tiempo, Dios guardaba su ropa y sandalias del desgaste.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-12.jpg?direct&}}

God even miraculously gave them water from a rock. But despite all this, the people of Israel complained and grumbled against God and against Moses. Even so, God was still faithful to his promises to Abraham, Isaac, and Jacob.

Dios aún les dió agua de una roca milagrosamente. Pero a pesar de todo esto, la gente de Israel se quejó y gruñó contra Dios y contra Moisés. Aun así, Dios todavía era fiel a sus promesas a Abraham, a Isaac, y a Jacobo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-13.jpg?direct&}}

Another time when the people did not have any water, God told Moses, "Speak to the rock, and water will come out of it." But Moses dishonored God in front of all the people by hitting the rock twice with a stick instead of speaking to it. Water came out of the rock for everyone to drink, but God was angry with Moses and said, "You will not enter the Promised Land."

En una ocasión cuando la gente no tenía nada de agua, Dios dijo a Moisés, "Habla a la roca, y agua saldrá de ella."  Pero Moisés deshonró a Dios en frente de toda la gente pegándole a la roca dos veces con un palo en lugar de hablarle.  Agua salió de la roca para que todos pudieran beber, pero Dios estaba enojado con Moisés y le dijo, "No entrarás en la Tierra Prometida."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-14.jpg?direct&}}

After the Israelites had wandered in the wilderness for forty years, all of them who had rebelled against God were dead. Then God led the people to the edge of the Promised Land again. Moses was now very old, so God chose Joshua to help him lead the people. God also promised Moses that one day, he would send another prophet like Moses.

Después que los israelitas vagaron en el desierto por cuarenta años, todos los que se habían rebelado contra Dios habían muerto. Entonces, Dios guió a la gente a la orilla de la Tierra Prometida otra vez. Moisés ahora era muy viejo, así que Dios escogió a Josué para ayudarle dirigir a la gente. También, Dios prometió a Moisés que un día, enviaría a otro profeta como Moisés.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-14-15.jpg?direct&}}

Then God told Moses to go to the top of a mountain so he could see the Promised Land. Moses saw the Promised Land but God did not permit him to enter it. Then Moses died, and the Israelites mourned for thirty days. Joshua became their new leader. Joshua was a good leader because he trusted and obeyed God.

Entonces, Dios dijo a Moisés que subiera a la cima de una montaña para que él pudiera ver la Tierra Prometida. Moisés vió la Tierra Prometida pero Dios no le permitió que la entrara.  Entonces, Moisés murió, y los israelitas endecharon por él​ por treinta días. Josué se convirtió en su nuevo líder. Josué era un buen líder porque él confiaba y obedecía a Dios.

//A Bible story from: Exodus 16-17; Numbers 10-14; 20; 27; Deuteronomy 34//

Una historia de la Biblia de: É//xodo 16-17; Números 10-14; 20; 27; Deuteronomío 34//
