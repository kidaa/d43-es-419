====== 40. Jesus Is Crucified ======

====== 40. Jesús Es Crucificado ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-01.jpg?direct&}}

After the soldiers mocked Jesus, they led him away to crucify him. They made him carry the cross on which he would die.

Después de burlarse ​de Jesús los soldados, le llevaron para cucificarlo. Le forzaron a cargar la cruz en que moriría.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-02.jpg?direct&}}

The soldiers brought Jesus to a place called "the Skull" and nailed his hands and feet to the cross. But Jesus said, "Father, forgive them, because they do not know what they are doing." Pilate commanded that they write, "King of the Jews" on a sign and put it on the cross above Jesus' head.

Los soldados llevaron a Jesús a un lugar que se llamaba "la Calavera" y clavaron sus manos y sus pies a la cruz. Pero Jesús dijo, "Padre, perdónales, porque no saben lo que están haciendo." Pilato mandó que escribieran, "Rey de los Judios" en un rótulo y lo colocaran en la cruz arriba de la cabeza de Jesús.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-03.jpg?direct&}}

The soldiers gambled for Jesus' clothing. When they did this, they fulfilled a prophecy that said, "They divided my garments among them, and gambled for my clothing."

Los soldados apostaron por la ropa de Jesús. Cuando lo hicieron, cumplieron la profecía que decía, "Ellos dividieron mi ropa entre sí, y apostaron por mi ropa."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-04.jpg?direct&}}

Jesus was crucified between two robbers. One of them mocked Jesus, but the other said, "Do you have no fear of God? We are guilty, but this man is innocent." Then he said to Jesus, "Please remember me in your kingdom." Jesus answered him, "Today, you will be with me in Paradise."

Jesús fue crucificado entre dos ladrones. Uno de ellos se burló de Jesús, pero el otro dijo, "¿No tienes miedo de Dios? Somos culpables, pero este hombre es inocente." Entonces, él dijo a Jesús, "Por favor, acuerdatede mi en tu reino." Jesús le contestó, "Hoy, estarás conmigo en el Paraíso."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-05.jpg?direct&}}

The Jewish leaders and the other people in the crowd mocked Jesus. They said to him, "If you are the Son of God, come down from the cross and save yourself! Then we will believe you."

Los líderes de los judíos y la otra gente en la multitud​ se burlaron de Jesús. Ellos le dijeron, "¡Si tú eres es el Hijo de Dios, bájate de esa cruz y sálvate a ti mismo! "Entonces, creeremos en tí."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-06.jpg?direct&}}

Then the sky over the whole region became completely dark, even though it was the middle of the day. It stayed dark from noon until 3:00 in the afternoon.

Entonces, el cielo sobre toda la región se oscureció completamente, aun siendo el medio día. Se mantuvo oscuro desde el mediodía hasta las 3:00 de la tarde.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-07.jpg?direct&}}

Then Jesus cried out, "It is finished! Father, I give my spirit into your hands." Then he bowed his head and gave up his spirit. When he died, there was an earthquake and the large curtain that separated the people from the presence of God in the Temple was torn in two, from the top to the bottom.

Entonces, Jesús gritó, "¡Está terminado!" Padre, entrego mi espíritu en tus manos." Entonces, inclinó su cabeza y entregó su espíritu. Cuando él murió, hubo un teremoto y la cortina grande en el templo​ que separaba la gente de la presencia de Dios se rasgó en dos, de arriba hasta abajo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-08.jpg?direct&}}

Through his death, Jesus opened a way for people to come to God. When the soldier guarding Jesus saw everything that had happened, he said, "Certainly, this man was innocent. He was the Son of God."

Por su muerte, Jesús abrió el camino para que la gente venga a Dios. Cuando el soldado que estaba guardando a Jesús vío todo lo que sucedió, dijo, "Ciertamente, este hombre era inocente. "Él era el Hijo de Dios."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-40-09.jpg?direct&}}

Then Joseph and Nicodemus, two Jewish leaders who believed Jesus was the Messiah, asked Pilate for Jesus' body. They wrapped his body in cloth and placed it in a tomb cut out of rock. Then they rolled a large stone in front of the tomb to block the opening.

Entonces, José y Nicodemo, dos líderes de los judíos que creían que Jesús era el Mesías, pidieron a Pilato el cuerpo de Jesús. Ellos envolvieron su cuerpo en tela y lo colocaron en una tumba cortada en la roca. Entonces, ellos rodaron una piedra grande frente al sepulcro para bloquear la abertura.

//A Bible story from: Matthew 27:27-61; Mark 15:16-47; Luke 23:26-56; John 19:17-42//

//Una historia de la Biblia de: Mateo 27:27-61; Marcos 15:16-47; Lucas 23:26-56; Juan 19:17-42//
