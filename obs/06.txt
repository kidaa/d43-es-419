====== 6. Dios Provee por Isaac ======

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-01.jpg?direct&}}

Cuando Abraham era muy viejo, su hijo, Isaac, había crecido hasta convertirse en un hombre. Así que Abraham envió a uno de sus siervos de nuevo a la tierra donde vivían sus parientes para traer de vuelta una esposa para su hijo Isaac.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-02.jpg?direct&}}

Después de un largo viaje a la tierra donde vivían los parientes de Abraham, Dios guió al siervo a Rebeca. Ella era la nieta del hermano de Abraham.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-03.jpg?direct&}}

Rebeca aceptó dejar a su familia y regresar con el siervo de la casa de Isaac. Isaac se casó con ella tan pronto como llegó.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-04.jpg?direct&}}

After a long time, Abraham died and all of the promises that God had made to him in the covenant were passed on to Isaac. God had promised that Abraham would have countless descendants, but Isaac's wife, Rebekah, could not have children.

Después de mucho tiempo, Abraham murió, y todas las promesas que Dios le había hecho a él en el pacto pasaron a Isaac. Dios había prometido a Abraham que tendría un sin número de descendientes, pero la esposa de Isaac, Rebeca, no podía tener a hijos.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-05.jpg?direct&}}

Isaac prayed for Rebekah, and God allowed her to get pregnant with twins. The two babies struggled with each other while they were still in Rebekah's womb, so Rebekah asked God what was happening.

Isaac oró por Rebeca, y Dios le permitió quedar embarazada de gemelos. Los dos bebés luchaban entre sí aún en el vientre de Rebeca, así que Rebeca preguntó a Dios lo que estaba sucediendo.

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-06.jpg?direct&}}

Dios le dijo a Rebeca: "Dos naciones vendrán de los dos hijos dentro de ti. Ellos lucharán entre sí y el hijo mayor servirá al menor de ellos."

{{https://api.unfoldingword.org/obs/jpg/1/en/360px/obs-en-06-07.jpg?direct&}}

When Rebekah's babies were born, the older son came out red and hairy, and they named him Esau. Then the younger son came out holding on to Esau's heel, and they named him Jacob.

Cuando nacieron los bebés de Rebeca, el hijo mayor salió rojo y peludo, y lo llamaron Esaú. Entonces el hijo menor salió agarrando el talón de Esaú, y lo llamaron Jacob.

//A Bible story from: Genesis 24:1-25:26//

//Una historia de la Biblia de: Génesis 24:1-25:26//
